import {createStore, applyMiddleware} from 'redux';
import reducers from '../reducers'
import createSagaMiddleware from 'redux-saga'
import { REDUX_GETDRINKS } from '../constans'

import {rootSaga} from '../sagas'

const initalState = {}
const sagaMiddleware = createSagaMiddleware()
const store = createStore(reducers, initalState, applyMiddleware(sagaMiddleware))
sagaMiddleware.run(rootSaga)

store.subscribe(() => {
  const auth = store.getState().auth
  localStorage.setItem(REDUX_GETDRINKS, JSON.stringify(auth))
})

// Dev only !!!!!
window.store = store

export default store
