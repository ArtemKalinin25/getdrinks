import { all, takeEvery, put } from 'redux-saga/effects'
import {appName, START, SUCCESS, ERROR} from '../../../globals'

const moduleName = `${appName}/CATALOG`

// Actions
const GET_PRODUCTS_BY_CATEGORY = `${moduleName}/GET_PRODUCTS_BY_CATEGORY`
const GET_PRODUCTS_BY_COLLECTION = `${moduleName}/GET_PRODUCTS_BY_COLLECTION`
const GET_PRODUCTS_BY_TAG_NAME = `${moduleName}/GET_PRODUCTS_BY_TAG_NAME`

// Reducer

const defaultState = {
  loading: false,
  loaded: false,
  errors: null,
  data: []
}

const reducerMap = {
  [GET_PRODUCTS_BY_CATEGORY + START]: (state) => ({
    ...state,
    loading: true
  }),
  [GET_PRODUCTS_BY_CATEGORY + SUCCESS]: (state, action) => ({
    loading: false,
    loaded: true,
    data: [ ...state.data, action.payload ],
    errors: null
  }),
  [GET_PRODUCTS_BY_CATEGORY + ERROR]: (state, action) => ({
    ...state,
    loading: false,
    errors: action.payload
  }),
}

export default (state = defaultState, action) => {
  const reducer = reducerMap[action.type]
  if (!reducer) {
    return state
  }

  return reducer(state, action)
}

// Action Creators
export const getProductsByCollection = (collection) => {
  return {
    type: GET_PRODUCTS_BY_CATEGORY,
    payload: collection
  }
}

// Sagas
export const rootSaga = function * () {
  yield all([])
}



// Selectors
