export const points = [
{
point_id: "00-МД012528",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Покрышкина ул, дом № 5",
coords: [
55.664436,
37.469216
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "МД00014027",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Судостроительная ул, дом № 37/11",
coords: [
55.684488,
37.685817
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "МД00009367",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Черкизовская Б. ул, дом № 6",
coords: [
55.796203,
37.719594
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-00000165",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Боровское ш., дом № 27",
coords: [
55.64475,
37.365748
],
internal_id: "",
point_type: "Магазин",
work_time: "с 10:00 до 23:00",
net_name: "Лит.Ra"
},
{
point_id: "00-00000169",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Ясеневая ул, дом № 32, корпус 3",
coords: [
55.602993,
37.740246
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-00000173",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Солнцевский пр-кт, дом № 2, строение 3",
coords: [
55.652578,
37.396865
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-00000182",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Народного Ополчения ул, дом № 29, корпус 1",
coords: [
55.781585,
37.479708
],
internal_id: "",
point_type: "Магазин",
work_time: "с 10:00 до 23:00",
net_name: "Лит.Ra"
},
{
point_id: "00-00000186",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Донецкая ул, дом № 23, строение 2",
coords: [
55.6473,
37.715534
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "МД00011059",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Шверника ул, дом № 13, корпус 1",
coords: [
55.691679,
37.589096
],
internal_id: "",
point_type: "Магазин",
work_time: "с 10:00 до 23:00",
net_name: "Лит.Ra"
},
{
point_id: "МД00008373",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Петровско-Разумовский проезд, дом № 16",
coords: [
55.797574,
37.569468
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "МД00007338",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Химкинский б-р, дом № 1",
coords: [
55.850328,
37.451168
],
internal_id: "",
point_type: "Магазин",
work_time: "с 10:00 до 23:00",
net_name: "Лит.Ra"
},
{
point_id: "00-00000166",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Судостроительная ул, дом № 17",
coords: [
55.681382,
37.674454
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-00000174",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Толбухина ул, дом № 8, корпус 1",
coords: [
55.719282,
37.401833
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-МД012529",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, п.Воскресенское, Чечёрский проезд, дом № 126",
coords: [
55.524355,
37.513961
],
internal_id: "",
point_type: "Магазин",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-00000181",
show_name: "Getdrinks Пункт выдачи ",
address: "Московская обл, г.Реутов, Юбилейный пр-кт, дом № 47",
coords: [
55.752561,
37.876404
],
internal_id: "",
point_type: "Магазин",
work_time: "с 10:00 до 23:00",
net_name: "Лит.Ra"
},
{
point_id: "00-00000185",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Гришина ул, дом № 16",
coords: [
55.722593,
37.418191
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "МД00007459",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Октябрьская ул, дом № 18",
coords: [
55.787285,
37.613997
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "МД00014045",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Малыгина ул, дом № 5, корпус 1",
coords: [
55.879526,
37.696588
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "МД00012220",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Паустовского ул, дом № 6, корпус 1",
coords: [
55.600318,
37.53906
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-00000167",
show_name: "Getdrinks Пункт выдачи ",
address: "Московская обл, г.Химки, Сходня мкр, Мичурина ул, дом № 17, помещение 2",
coords: [
55.945503,
37.300485
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-00000171",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, п.Сосенское, п.Коммунарка, Александры Монаховой ул, дом № 96, корпус 2",
coords: [
55.540949,
37.486544
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-00000177",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Рождественская ул, дом № 31",
coords: [
55.705914,
37.931722
],
internal_id: "",
point_type: "Магазин",
work_time: "с 10:00 до 23:00",
net_name: "Лит.Ra"
},
{
point_id: "00-00000178",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Юрьевский пер, дом № 22, корпус 1",
coords: [
55.76253,
37.717447
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-00000188",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Сиреневый б-р, дом № 44",
coords: [
55.802583,
37.815471
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "МД00008914",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Вешняковская ул, дом № 39Г",
coords: [
55.718136,
37.826161
],
internal_id: "",
point_type: "Магазин",
work_time: "с 10:00 до 23:00",
net_name: "Лит.Ra"
},
{
point_id: "МД00008912",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Родионовская ул, дом № 12",
coords: [
55.893854,
37.389607
],
internal_id: "",
point_type: "Магазин",
work_time: "с 10:00 до 23:00",
net_name: "Лит.Ra"
},
{
point_id: "МД00010983",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Щелковское ш, дом № 12, корпус 1",
coords: [
55.806105,
37.770079
],
internal_id: "",
point_type: "Магазин",
work_time: "с 10:00 до 23:00",
net_name: "Лит.Ra"
},
{
point_id: "00-00000170",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Десеновское п, Нововатутинская 5-я ул, дом № 9",
coords: [
55.516379,
37.347368
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-00000172",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, п.Сосенское, п.Коммунарка, Лазурная ул, дом № 11",
coords: [
55.56588,
37.472629
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-00000180",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Яна Райниса б-р, дом № 8",
coords: [
55.850899,
37.427318
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-00000184",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Гражданская 3-я ул, дом № 70",
coords: [
55.807956,
37.713566
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-00000187",
show_name: "Getdrinks Пункт выдачи ",
address: "Московская обл, г.Королев, Юбилейный мкр, М.К.Тихонравова ул, дом № 35, корпус 7",
coords: [
55.933383,
37.842735
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "МД00009728",
show_name: "Getdrinks Пункт выдачи ",
address: "Московская обл, г.Химки, Московская ул, дом № 7/1",
coords: [
55.892006,
37.448222
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "МД00007404",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Летчика Бабушкина ул, дом № 10/1",
coords: [
55.862171,
37.674606
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "МД00001134",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Шокальского проезд, дом № 61",
coords: [
55.883989,
37.666782
],
internal_id: "",
point_type: "Магазин",
work_time: "с 10:00 до 23:00",
net_name: "Лит.Ra"
},
{
point_id: "00-00000164",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Маршала Жукова пр-кт, дом № 17",
coords: [
55.775373,
37.492751
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-МД012530",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Кронштадтский б-р, дом № 47",
coords: [
55.851717,
37.511724
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-00000179",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Ясеневая ул, дом № 12, корпус 1",
coords: [
55.601203,
37.727553
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "00-00000183",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Ленинский пр-кт, дом № 89/2",
coords: [
55.677935,
37.531622
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
},
{
point_id: "МД00007448",
show_name: "Getdrinks Пункт выдачи ",
address: "Москва, Серпуховская Б. ул, дом № 48/1",
coords: [
55.721944,
37.626987
],
internal_id: "",
point_type: "Магазин-бар",
work_time: "с 10:00 до 23:00",
net_name: "Хмель&Солод"
}
]
