import { LOGIN_USER, REGISTER_USER, START, SUCCESS, FAIL } from '../constans'

const getDefaultState = () => {
  const authStorage = JSON.parse(localStorage.getItem('REDUX_GETDRINKS'))
  if (authStorage && authStorage.loaded) {
    return authStorage
  } else {
    return {
      loading: false,
      loaded: false,
      errors: null,
      errorText: null,
      entitys: []
    }
  }
}

/*const loginDefaultState = {
  loading: false,
  loaded: false,
  errors: null,
  errorText: null,
  entitys: []
}*/

export default (loginState = getDefaultState(), actions) => {
  const {type, payload} = actions

  switch (type) {
    case LOGIN_USER + START:
      return { ...loginState, loading: true }

    case REGISTER_USER + SUCCESS:
    case LOGIN_USER + SUCCESS:
      console.log(LOGIN_USER + SUCCESS, payload)
      return { ...loginState, loading: false, loaded: true, entitys: payload }

    case LOGIN_USER + FAIL:
      console.log(LOGIN_USER + FAIL, payload)
      return { ...loginState, loading: false, errors: true, errorText: payload }

    default:
      return loginState
  }
}
