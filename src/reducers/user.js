import { GET_USER_DATA, START, SUCCESS, FAIL, CLEAR_REG_ERRORS } from '../constans'

const userDefaultState = {
  loading: false,
  loaded: false,
  errors: null,
  data: null
}

export default (userState = userDefaultState, actions) => {

  const { type, payload } = actions

  switch (type) {
    case GET_USER_DATA + START:
      return { ...userState, loading: true }

    case GET_USER_DATA + SUCCESS:
      return { ...userState, loading: false, loaded: true, data: payload }

    case GET_USER_DATA + FAIL:
      return { ...userState, loading: false, errors: true }

    case CLEAR_REG_ERRORS:
      console.log("REDUCER ", CLEAR_REG_ERRORS)
      return { ...userState, errors: false, data: null }

    default:
      return userState
  }

}
