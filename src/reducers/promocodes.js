import { CHECK_PROMOCODE, START, SUCCESS, FAIL } from '../constans'

const promocodeDefaultState = {
  loadind: false,
  loaded: false,
  errors: null,
  entitys: []
}

export default (promoState = promocodeDefaultState, actions) => {

  const { type, payload } = actions

  switch (type) {
    case CHECK_PROMOCODE + START:
      return { ...promoState, loading: true }

    case CHECK_PROMOCODE + SUCCESS:
      console.log(CHECK_PROMOCODE + SUCCESS, payload)
      return { ...promoState, loading: false, loaded: true, entitys: payload }

    case CHECK_PROMOCODE + FAIL:
      return { ...promoState, errors: true }

    default:
      return promoState
  }

}
