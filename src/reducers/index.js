import {combineReducers} from 'redux'

import cart from './cart'
import products from './products'
import categories from './categories'
import pickpoints from './pickuppoints'
import recomendations from './recomendations'
import tags from './tags'
import promocodes from './promocodes'
import register from './register'
import auth from './auth'
import user from './user'
import llegal from './llegal'


import filterProducts from './filterProduct'

export default combineReducers({
  cart, products,
  filterProducts, categories,
  pickpoints, recomendations,
  tags, promocodes, register, auth, user, llegal
})
