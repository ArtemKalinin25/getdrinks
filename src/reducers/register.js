import { REGISTER_USER, START, SUCCESS, FAIL } from '../constans'


const registerDefaultState = {
  loading: false,
  loaded: false,
  errors: null,
  responce: null
}
 
export default (registerState = registerDefaultState, actions) => {
  const { type, payload } = actions

  switch (type) {
    case REGISTER_USER + START:
      return { ...registerState, loading: true }

    case REGISTER_USER + SUCCESS:
      return { ...registerState, loading: false, loaded: true, responce: payload }

    case REGISTER_USER + FAIL:
      return { ...registerState, loading: false, errors: true, responce: payload }

    default:
      return registerState
  }
}
