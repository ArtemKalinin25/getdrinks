import {OrderedMap, Map, Record} from 'immutable'
import {arrToMap, mapToArr} from '../helpers/converters'

import {
  GET_PRODUCTS_BY_CATEGORY,
  GET_PRODUCT_ITEM,
  GET_PRODUCTS_BY_SUBCATEGORY,
  START,
  SUCCESS,
  FAIL,
  GET_PRODUCTS_BY_COLLECTION,
  GET_PRODUCTS_BY_CATEGORY__API,
  GET_ONE_SCU_BY_PRODUCTS__API,
  GET_PRODUCTS_BY_SUBCATEGORY__API,
  GET_PRODUCTS_BY_COLLECTION__API,
  GET_PRODUCT_BY_TAG_NAME,
  GET_FILTERS_PRODUCTS,
  FILTER_COUNTRY_PRODUCTS,
  FILTER_CATEGORY_PRODUCTS,
  FILTER_BRAND_PRODUCTS,
  FILTER_VALUE_ALC,
  FILTER_MIN_VOLUME_CHANGE,
  FILTER_MAX_VOLUME_CHANGE,
  FILTER_SELECT_PACK,
  FILTER_MIN_PACK_CHANGE,
  FILTER_MAX_PACK_CHANGE,
  FILTER_SELECT_TYPE_PACK,
  GET_SELECT_ALL_FILTERS
} from '../constans'


const ProductsDefaultState = {
  loading: false,
  loaded: false,
  errors: null,
  entitys: [],
  dataFilter: null,
  filterLoading: false,
  alcVolumeProduct: { min: 0, max: 10 },
  selectPack: { min: 2, max: 24 },
  selectAllFilters: null
}

export default (productsState = ProductsDefaultState, action) => {
 
  const {type, json, payload} = action

  switch (type) {
    case GET_PRODUCTS_BY_CATEGORY__API + START:
      //console.log(GET_PRODUCTS_BY_CATEGORY__API + START)
      return { ...productsState, loading: true }

    case GET_PRODUCTS_BY_CATEGORY__API + SUCCESS:
      //console.log(GET_PRODUCTS_BY_CATEGORY__API + SUCCESS)
      return { ...productsState, loading: false, loaded: true, entitys: payload }

    case GET_PRODUCTS_BY_CATEGORY__API + FAIL:
      //console.log(GET_PRODUCTS_BY_CATEGORY__API + FAIL)
      return { ...productsState, loading: false, errors: true }

    case GET_ONE_SCU_BY_PRODUCTS__API + START:
      //console.log(GET_ONE_SCU_BY_PRODUCTS__API + START)
      return { ...productsState, loading: true }

    case GET_ONE_SCU_BY_PRODUCTS__API + SUCCESS:
      //console.log(GET_ONE_SCU_BY_PRODUCTS__API + SUCCESS, payload)
      return { ...productsState, loading: false, loaded: true, entitys: payload }

    case GET_ONE_SCU_BY_PRODUCTS__API + FAIL:
      //console.log(GET_ONE_SCU_BY_PRODUCTS__API + FAIL)
      return { ...productsState, loading: false, errors: true }

    case GET_PRODUCTS_BY_SUBCATEGORY__API + START:
      return { ...productsState, loading: true }

    case GET_PRODUCTS_BY_SUBCATEGORY__API + SUCCESS:
      return { ...productsState, loading: false, loaded: true, entitys: payload }

    case GET_PRODUCTS_BY_SUBCATEGORY__API + FAIL:
      return { ...productsState, loading: false, errors: true }

    case GET_PRODUCTS_BY_COLLECTION__API + START:
      //console.log(GET_PRODUCTS_BY_COLLECTION__API + START)
      return { ...productsState, loading: true }

    case GET_PRODUCTS_BY_COLLECTION__API + SUCCESS:
      return { ...productsState, loading: false, loaded: true, entitys: payload }

    case GET_PRODUCTS_BY_COLLECTION__API + FAIL:
      return { ...productsState, loading: false, errors: true }

    case GET_PRODUCT_BY_TAG_NAME + START:
        //console.log(GET_PRODUCTS_BY_COLLECTION__API + START)
      return { ...productsState, loading: true }

    case GET_PRODUCT_BY_TAG_NAME + SUCCESS:
      return { ...productsState, loading: false, loaded: true, entitys: payload }

    case GET_PRODUCT_BY_TAG_NAME + FAIL:
      return { ...productsState, loading: false, errors: true }
    //Action получает данные для фильтра
    case GET_FILTERS_PRODUCTS:
      return {
        ...productsState,
        filterLoading: true,
        dataFilter: {...action.payload}
      }
    //Action фильтрует выбранные фильтры по Странам
    case FILTER_COUNTRY_PRODUCTS:
      let countrySelect = action.payload;
      let newDataFilterCountry = productsState.dataFilter;
      const resultCountry = newDataFilterCountry.country.filter(item => item.countryName === countrySelect);
      resultCountry[0].filterSelect = !resultCountry[0].filterSelect;
      return  {
        ...productsState,
        dataFilter: newDataFilterCountry
      }

    //Фильтр фильтрует выбранные фильтры по Категориям
    case FILTER_CATEGORY_PRODUCTS:
      let categorySelect = action.payload;
      let newDataFilterCategory = productsState.dataFilter;
      const resultCategory = newDataFilterCategory.sortArrCategory.filter(item => item.subcategoryName === categorySelect);
      resultCategory[0].filterSelectCategory = !resultCategory[0].filterSelectCategory;
      console.log(resultCategory)
      return {
        ...productsState,
        dataFilter: newDataFilterCategory
      };
    //Фильтр выбранные по Бренду
    case FILTER_BRAND_PRODUCTS:
      let brandSelect = action.payload;
      let newDataFilterBrands = productsState.dataFilter;
      const resultBrand = newDataFilterBrands.sortArrCategory.filter(item => item.brandId === brandSelect); //Сейчас brandId не зависит от выбранной категории (а должен быть уникальным для каждой катеогории)
      resultBrand[0].filterSelect = !resultBrand[0].filterSelect;
      console.log(resultBrand);
      return {
        ...productsState,
        dataFilter: newDataFilterBrands
      };
    //Фильтр по содержанию Алкоголя в товаре
    case FILTER_VALUE_ALC:
      let newAlcVolume = productsState.alcVolumeProduct;
      newAlcVolume = {
        min: action.payload.min,
        max: action.payload.max,
      }
      return {
        ...productsState,
        alcVolumeProduct: newAlcVolume
      };
    //Фильтр по минимальному содержанию алкоголя в товаре
    case FILTER_MIN_VOLUME_CHANGE:
      let newAlcVolumeMin = productsState.alcVolumeProduct;
      newAlcVolumeMin = {
        min: action.minVolume,
        max: action.maxVolume
      }
      return {
        ...productsState,
        alcVolumeProduct: newAlcVolumeMin
      }
    //Фильтр по макисмальному содержанию алкоголя в товаре
    case FILTER_MAX_VOLUME_CHANGE:
      let newAlcVolumeMax = productsState.alcVolumeProduct;
      newAlcVolumeMax = {
        min: productsState.alcVolumeProduct.min,
        max: action.maxVolume
      }
      return {
        ...productsState,
        alcVolumeProduct: newAlcVolumeMax
      }
    //Фильтр количества упаковок
    case FILTER_SELECT_PACK:
      let newSelectPack = productsState.selectPack
      newSelectPack = {
        min: action.payload.min,
        max: action.payload.max,
      }
      return {
        ...productsState,
        selectPack: newSelectPack
      }
    //Фильтр выбора минимального количества товара в упаковке
    case FILTER_MIN_PACK_CHANGE:
      let newSelectPackMin = productsState.selectPack;
      newSelectPackMin = {
        min: action.minPack,
        max: action.maxPack
      }
      return {
        ...productsState,
        selectPack: newSelectPackMin
      }
    //Фильтр выбора максимального количества товара в упаковке
    case FILTER_MAX_PACK_CHANGE:
      let newSelectPackMax = productsState.selectPack;
      newSelectPackMax = {
        min: productsState.selectPack.min,
        max: action.maxPack
      }
      return {
        ...productsState,
        selectPack: newSelectPackMax
      }
    //Фильтр по типу упаковки товара
    case FILTER_SELECT_TYPE_PACK:
      let packSelect = action.payload;
      let newDataFilterPackType = productsState.dataFilter;
      const resultTypePack = newDataFilterPackType.typePack.filter(item => item.packageNameId === packSelect);
      resultTypePack[0].filterSelect = !resultTypePack[0].filterSelect;
      console.log(resultTypePack)
      return {
        ...productsState,
        dataFilter: newDataFilterPackType
      }
    //Получает все выбранные фильтры
    case GET_SELECT_ALL_FILTERS:
      let newSelectAllFilters;
      if (productsState.dataFilter) {
        let nameObjValues = Object.values(productsState.dataFilter)
        let flatArray = nameObjValues.flat();
        let fullSelectFilters = flatArray.filter(item => item.filterSelect === true || item.filterSelectCategory === true);
        newSelectAllFilters = fullSelectFilters
      }
      return {
        ...productsState,
        selectAllFilters: {
          filters: newSelectAllFilters,
          alcoholVolumeSelect: productsState.alcVolumeProduct,
          selectPackProduct: productsState.selectPack
        }
      }
    default:
      return productsState
  }
}




//import {newProducts} from '../fakeData/newFakeProducts'

/*
//import {finalProducts} from '../fakeData/finalProduct'
import {last} from '../fakeData/lastFake'
//const products = arrToMap(newProducts)
const products = last

//console.log("last", last)

/*const sortByCategory = (arr, category) => {
  return arr.filter(item => {
    if (item.category_id == category) {
      return item
    }
  })
}*/

//const sortingProducts = (arr, row, sorted) => arr.filter( item => item[row] == sorted ? item : false )
//const oneProduct = (arr, productId) => arr.filter( item => item.scu_id == productId ? item : false )

//const getProdyctsByCollection = (arr, collectionId) => arr.filter( item => item.collection == collectionId ? item : false )


/*const productRecord = new Record({
  scu_id: undefined,
  scu_name: undefined,
  md_id: undefined,
  brand_name: undefined,
  category_id: undefined,
  country_name: undefined,
  mbc_id: undefined,
  pack_quantity: undefined,
  price_by_one: undefined,
  subcategory_id: undefined,
  subcategory_name: undefined,
})

const ReducerProductState = new Record({
  loading: false,
  loaded: false,
  entitys: arrToMap(products, productRecord),
  fail: false
})*/

/*const ReducerProductState = {
  loading: false,
  loaded: false,
  entitys: products
}

export default (productState = ReducerProductState, action) => {
  const {type, json, payload} = action

  switch(type) {
    case GET_PRODUCTS_BY_CATEGORY:
      //console.log("GET_PRODUCTS_BY_CATEGORY", productState)
      return { ...productState, entitys: sortingProducts(productState.entitys, "category_id", payload) }
    case GET_PRODUCT_ITEM:
      return { ...productState, entitys: sortingProducts(productState.entitys, "scu_id", payload) }
    case GET_PRODUCTS_BY_SUBCATEGORY:
      return { ...productState, entitys: sortingProducts(productState.entitys, "subcategory_id", payload) }
    case GET_PRODUCTS_BY_COLLECTION:
      return { ...productState, entitys: sortingProducts(productState.entitys, "collections", payload) }
    default:
      return productState
  }
}
*/
