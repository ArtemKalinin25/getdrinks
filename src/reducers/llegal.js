import { GET_LLEGAL_DATA_BY_ID, START, SUCCESS, FAIL } from '../constans'

const llegalDefaultState = {
  loading: false,
  loaded: false,
  errors: null,
  entitys: []
}

export default (llegalState = llegalDefaultState, actions) => {

  const { type, payload } = actions

  switch (type) {
    case GET_LLEGAL_DATA_BY_ID + START:
      return { ...llegalState, loading: true }

    case GET_LLEGAL_DATA_BY_ID + SUCCESS:
      return { ...llegalState, loading: false, loaded: true, entitys: payload }

    case GET_LLEGAL_DATA_BY_ID + FAIL:
      return { ...llegalState, loading: false, errors: true }

    default:
      return llegalState
  }
}
