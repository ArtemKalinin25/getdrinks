import {OrderedMap, Map, Record} from 'immutable'
import {arrToMap} from '../helpers/converters'

import {GET_WATER_PRODUCTS_LIST} from '../constans'
import {waterProducts} from '../fakeData/newFakeProducts'

const products = arrToMap(waterProducts)


const ReducerWaterProductState = new Record({
  loading: false,
  loaded: false,
  entitys: products,
  fail: false
})

export default (waterProductState = ReducerWaterProductState, action) => {
  const {type, json, payload} = action

  switch(type) {
    case GET_WATER_PRODUCTS_LIST:
      console.log("GET_WATER_PRODUCTS_LIST")
      return waterProductState

  }

  return waterProductState
}
