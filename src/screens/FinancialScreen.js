import React, {Component} from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import styled, {ThemeProvider} from 'styled-components'

import { getLlegalDataById } from '../AC'
import HeaderComponent from '../components/common/HeaderComponent'
import FooterComponent from '../components/common/FooterComponent'


import {theme} from '../components/kit/theme'
import {Grid, Row, Col, Wrapper} from '../components/kit/Grid'

const Header = styled.h3`
  margin-top: 20px;
  font-size: 24px;
`

const SubHeader = styled.h4`
  margin-top: 10px;
`

const Point = styled.p`
  margin-left: 10px;
  margin-bottom: 7px;
`

const InfoBlock = styled.div`
  height: 400px;
  margin-top: 30px;
`

const Item = styled.p`
  margin-bottom: 5px;
  margin-top: 5px;
`

class FinancialScreen extends Component { // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    this.props.getLlegalDataById('financial')
  }

  render() {
    const { loading, loaded, errors, entitys } = this.props.llegal

    return (
      <ThemeProvider theme={theme}>
        <HeaderComponent />
          <Wrapper>
            { loading ? (<h1>Загрузка</h1>) : (<></>) }
            { errors ? (<h1>Ошибка загрузки</h1>) : (<></>) }

            { loaded ?
              (<div>
                  <Header>{entitys.showname}</Header>
                  <div>
                    <pre style={{ width: '100%',
                      whiteSpace: 'pre-line',
                      fontFamily: 'Roboto, Arial, sans-serif',
                      fontSize: '16px', lineHeight: '24px'}}>
                    {
                      entitys.description
                    }
                  </pre>
                  </div>
              </div>)
              : (<></>) }

          </Wrapper>
        <FooterComponent />
      </ThemeProvider>
    );
  }
}

FinancialScreen.propTypes = {
  //prop: PropTypes.type.isRequired
}

export default connect((state) => {
  return {
    llegal: state.llegal
  }
}, { getLlegalDataById })(FinancialScreen);
