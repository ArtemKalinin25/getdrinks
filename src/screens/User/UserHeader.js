import React from 'react'
import {Grid, Row, Col, media} from '../../components/kit/Grid'
import { HeaderWrapper } from './styled'

import { Button } from './styled/controls'

export const UserHeader = (props) => {
  return(
    <HeaderWrapper>
      <Row>
        <Col size={10}>
          <h3>Личный кабинет</h3>
        </Col>
        <Col size={2}>
          <Button>Пригласить друга</Button>
        </Col>
      </Row>
    </HeaderWrapper>
  )
}
