import styled from 'styled-components'
import { theme } from '../../../components/kit/theme'

export const Button = styled.div`
  display: flex;
  background: ${theme.color.green};
  border-radius: 5px;
  font-weight: bold;
  align-items: center;
  min-height: 40px;
  font-size: 14px;
  cursor: pointer;
  justify-content: center;
  margin-top: -10px;
`
