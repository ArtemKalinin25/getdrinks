import styled from 'styled-components'
import { theme } from '../../../components/kit/theme'

export const ItemDate = styled.h2`
  font-weight: 500;
  font-size: 24px;
  margin-bottom: 5px;
  position: relative;
  display: inline-block;
  cursor: pointer;

  :after {
    content: '';
    position: absolute;
    left: 105%;
    bottom: 5px;
    border: 7px solid transparent;
    ${(props) => props.show ? (
        `border-left: 7px solid ${theme.color.black};`
    ) : (
        `border-top: 7px solid ${theme.color.black};`
    )}

  }
`

export const ItemBlock = styled.div`
  box-sizing: border-box;
  padding: 5px;
  background: ${theme.color.gray};
  width: 90%;
  position: relative;
  min-width: 500px;
  margin-bottom: 20px;
`

export const StatusCol = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 10px;
  background: ${theme.color.green};
`

export const ItemHeader = styled.div`
  border-bottom: 1px solid ${theme.color.darkGray};
  box-sizing: border-box;
  padding: 10px;
  padding-left: 20px;
  margin-bottom: 10px;
`

export const ItemInfo = styled.div`
  box-sizing: border-box;
  padding: 10px;
  padding-left: 20px;
  margin-bottom: 10px;
`

export const ItemInfo_Point = styled.span`
  color: ${theme.color.darkGray};
  display: inline-block;
  margin-right: 10px;
`

export const ItemInfo_Text = styled.span`
  font-weight: 500;
`

export const ItemOrdersBlock = styled.div`
  box-sizing: border-box;
  border-top: 1px solid ${theme.color.darkGray};
  padding: 10px;
  padding-left: 20px;
  margin-bottom: 10px;
  display: ${props => props.show ? 'block' : 'none' }
`

export const ItemOrderElem = styled.td`
  width: ${props => props.width || '20%'};
  vertical-align: top;
`

export const ItemOrderHeader = styled.p`
  color: ${theme.color.darkGray};
`

export const ItemHeaderValue = styled.h4`
  font-weight: 500;
  font-size: 21px;
`
