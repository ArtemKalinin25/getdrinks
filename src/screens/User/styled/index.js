import styled from 'styled-components'
import { theme } from '../../../components/kit/theme'

export const HeaderWrapper = styled.div`
  height: 80px;
  box-sizing: border-box;
  padding: 25px;
  margin-bottom: 20px;
  padding-left: 60px;
  text-transform: uppercase;
  border: 1px solid ${theme.color.gray};
  font-family: 'Fira Sans Extra Condensed', sans-serif;
`

export const MenuWrapper = styled.ul`
  box-siing: border-box;
  padding: 25px;
  padding-left: 60px;
  margin-right: 40px;
`

export const MenuItem = styled.li`
  list-style-type: none;
  box-sizing: border-box;
  margin-bottom: 20px;
  margin-top: 20px;
`

export const MLink = styled.a`
  text-decoration: none;
  font-weight: 500;
  text-transform: uppercase;
  font-size: 18px;

  :hover {
    border-bottom: 2px solid ${theme.color.green};
  }
`

export const SubHeader = styled.h3`
  margin-top: 30px;
  margin-bottom: 15px;
  font-weight: 500;
  font-size: 21px;
`
