import React, { useState } from 'react'
import { Row, Col } from '../../../components/kit/Grid'
import { ItemBlock, ItemHeader,
  StatusCol, ItemInfo,
  ItemInfo_Point, ItemInfo_Text,
  ItemOrderElem, ItemOrdersBlock, ItemOrderHeader, ItemHeaderValue, ItemDate } from '../styled/order'

export const OrderItem = (props) => {
  const [isShowOrders, setIsShowOrders] = useState(false)

  const toogleShowOrders = () => {
    setIsShowOrders(!isShowOrders)
  }

  return(
    <ItemBlock>
      <StatusCol />
      <ItemHeader>
        <Row>
          <Col size={8}>
            <ItemDate onClick={toogleShowOrders} show={isShowOrders}>20 апреля 2020</ItemDate>
            <p>Уже едет к тебе!</p>
          </Col>
          <Col size={2}>
            <h3>Повторить заказ</h3>
          </Col>
        </Row>
      </ItemHeader>

      <ItemInfo>
        <Row>
          <Col size={4}>
            <p>
              <ItemInfo_Point>Номер заказа: </ItemInfo_Point>
              <ItemInfo_Text> #230925</ItemInfo_Text>
            </p>
            <p>
              <ItemInfo_Point>Сумма заказа: </ItemInfo_Point>
              <ItemInfo_Text> 3 560 р</ItemInfo_Text>
            </p>
          </Col>
          <Col size={8}>
            <p>
              <ItemInfo_Point>Ожидаемая дата доставки: </ItemInfo_Point>
              <ItemInfo_Text> 22 апреля </ItemInfo_Text>
            </p>
            <p>
              <ItemInfo_Point>Адрес доставки: </ItemInfo_Point>
              <ItemInfo_Text> Москва, 2-ой Красносельский пер., 2 </ItemInfo_Text>
            </p>
          </Col>
        </Row>
      </ItemInfo>

      <ItemOrdersBlock show={isShowOrders}>
        <table>
          <thead>
            <tr>
              <ItemOrderElem>
                <ItemOrderHeader>Артикул</ItemOrderHeader>
              </ItemOrderElem>
              <ItemOrderElem>
                <ItemOrderHeader>Цена за шт.</ItemOrderHeader>
              </ItemOrderElem>
              <ItemOrderElem>
                <ItemOrderHeader>Количество</ItemOrderHeader>
              </ItemOrderElem>
              <ItemOrderElem>
                <ItemOrderHeader>Сумма</ItemOrderHeader>
              </ItemOrderElem>
            </tr>
          </thead>
          <tbody>
            <tr>
              <ItemOrderElem>
                <ItemHeaderValue>Жигули Барное 0,5 л</ItemHeaderValue>
                <ItemOrderHeader>МД-0985687</ItemOrderHeader>
              </ItemOrderElem>
              <ItemOrderElem>
                <ItemHeaderValue>99 р</ItemHeaderValue>
              </ItemOrderElem>
              <ItemOrderElem>
                <ItemHeaderValue>24 шт</ItemHeaderValue>
              </ItemOrderElem>
              <ItemOrderElem>
                <ItemHeaderValue>2 376 р.</ItemHeaderValue>
              </ItemOrderElem>
            </tr>
            <tr>
              <ItemOrderElem>
                <ItemHeaderValue>Жигули Барное 0,5 л</ItemHeaderValue>
                <ItemOrderHeader>МД-0985687</ItemOrderHeader>
              </ItemOrderElem>
              <ItemOrderElem>
                <ItemHeaderValue>99 р</ItemHeaderValue>
              </ItemOrderElem>
              <ItemOrderElem>
                <ItemHeaderValue>24 шт</ItemHeaderValue>
              </ItemOrderElem>
              <ItemOrderElem>
                <ItemHeaderValue>2 376 р.</ItemHeaderValue>
              </ItemOrderElem>
            </tr>
          </tbody>
        </table>
        <div>
          <h4>Сообщить о проблеме с заказом</h4>
        </div>
      </ItemOrdersBlock>

    </ItemBlock>
  )
}
