import React, {Component} from 'react'
import PropTypes from 'prop-types'
import styled, {ThemeProvider} from 'styled-components'
import {theme} from '../components/kit/theme'
import {Col, Row} from '../components/kit/Grid'
import {Label} from '../components/common/Cart/styled-components/index'
import Header from '../components/common/HeaderComponent'
import FooterComponent from '../components/common/FooterComponent'
import { points } from '../fakeData/points'

const Wrapper = styled.div`
  margin-left: 60px;
  margin-right: 50px;
  @media (max-width: ${theme.sizes.mobileL}) {
    margin-left: 0;
    margin-right: 0;
  }
`

const coordsabc = points

const mapDataq = {
  center: [55.691218, 37.619684],
  zoom: 6,
};

const MapThis = styled.div`
  height: 200px;
  width: 100%;
  background: url('/img/common/tempMap.jpg') no-repeat;
  margin-top: 20px;
`


const PickUpChoise = styled.div`
  margin: 10px;
  margin-left: 0px;
  background: ${props => props.theme.color.gray};
  padding: 10px;
`

const PickUpChoise_Header = styled.p`
  font-weight: bold;
  margin-bottom: 5px;
`

const PickUpPointsList = styled.div`
  margin-right: 10px;
  overflow-x: auto;
  height: 280px;
`

const PickUpListItem = styled.div`
  padding: 5px;
  padding-top: 9px;
  padding-bottom: 9px;
  border-top: 1px solid ${props => props.theme.color.gray};
  border-bottom: 1px solid ${props => props.theme.color.gray};
  cursor: pointer;
  :hover {
    background: ${props => props.theme.color.gray};
  }
`

class PointsComponent extends Component { // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    //this.props.getAllPickPoints()
    window.addEventListener('load', this.handleLoad);
  }

  handleLoad = () => {
    window.ymaps.ready(() => {
      this.localMap = new window.ymaps.Map('mapa', {center: [55.928345, 37.766711], zoom: 9}, {
        searchControlProvider: 'yandex#search'});
        console.log("LOAD THIS")
        for (let i = 0; i < points.length; i++) {
          var placemark = new window.ymaps.Placemark(points[i].coords, {
              iconContent: points[i].point_id,
              hintContent : points[i].net_name + " " + points[i].address
            }, {
              iconLayout: 'default#image',
              iconImageHref: 'img/common/mapicon.gif',
              iconImageSize: [30, 30],
              iconImageOffset: [-5, -28]
          });
          placemark.name = points[i].address;
          placemark.events.add(['click'],  (e) => {
            const address = e.originalEvent.target.name
            const name = e.originalEvent.target.properties._data.hintContent
            const id = e.originalEvent.target.properties._data.iconContent

            console.log("ADRESS", e.originalEvent.target)
            //this.props.pickPointHandler(id, address, name)
          })

          this.localMap.geoObjects.add(placemark);
        }
    });
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <Header />
        <Wrapper>

        <Col size={2}>
          <h1>Пункты продаж</h1>
          <div id='mapa' style={{width: '100%', height: '400px'}}></div>
        </Col>
        <Col size={2}>
        <div>
          <p>Пункты продаж</p>
          <PickUpPointsList>
            {
              points.map(item => {
                return(
                  <PickUpListItem
                    ref={this.pickUpItem}
                    data-id={item.point_id}
                  >

                    <Label>{item.point_type} {item.net_name}</Label>
                    <Label>Время работы {item.work_time}</Label>
                    <p>{item.address}</p>
                  </PickUpListItem>
                )
              })
            }
          </PickUpPointsList>
        </div>
      </Col>
        </Wrapper>
        <FooterComponent />
      </ThemeProvider>
    );
  }
}

export default PointsComponent
