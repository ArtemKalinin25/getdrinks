import React, { useState } from 'react'
//import PropTypes from 'prop-types'
import styled, {ThemeProvider} from 'styled-components'

import { theme } from '../../components/kit/theme'
import {Grid, Row, Col} from '../../components/kit/Grid'
import PromoMenuBlock from './PromoMenuBlock'
//import OurBenefitsComponent from '../../components/common/OurBenefitsComponent'
import HowToOrderComponent from '../../components/common/HowToOrderComponent'
//import BenefitsCopy from '../../components/common/BenefitsCopy'
import OurResponsibility from '../../components/common/OurResponsibility'
import LoginModal from '../../components/common/Modals/LoginModal'

export const Wrapper = styled.div`
  margin-left: 60px;
  margin-right: 50px;
  @media (max-width: ${theme.sizes.mobileL}) {
    margin-right: 1px;
    margin-left: 20px;
  }
`

const PromoInfo = styled.div`
  margin-top: 50px;
  margin-right: 60px;
  @media (max-width: ${theme.sizes.mobileL}) {
    margin-right: 1px;
    width: 95%;
  }
`;

const PromoInfoHeader = styled.h1`
  margin-bottom: 10px;
  text-transform: uppercase;
  font-weight: bold;
`;

const SimpleLogo = styled.div`
  background: url('/img/logo/simpleLogo.svg') no-repeat;
  display: inline-block;
  width: 235px;
  height: 35px;
  margin-top: 9px;
  margin-right: 10px;
  margin-bottom: -5px;
  background-size: contain;
  @media (max-width: ${theme.sizes.mobileL}) {
    width: 80%;
  }
`

const PromoText = styled.p`
  font-family: 'Oswald';
  font-size: 20px;
`

const Selection = styled.span`
  border-bottom: 2px dashed ${theme.color.darkGray};
  cursor: pointer;

  :hover {
      background: ${theme.color.green};
  }
`

const MainPageContent = (props) => {
  const [isLoginModalOpen, toogleLoginModal] = useState(false)

  const onClickLoginBtnHandler = () => {
    toogleLoginModal(!isLoginModalOpen)
  }

  return (
      <Wrapper>
        <Row>
          <Col size={4} collapse="xs">
          </Col>
          <Col size={8}>
            <PromoInfo>
              <PromoInfoHeader><SimpleLogo />
                <Selection onClick={onClickLoginBtnHandler}>Зарегистрируйся</Selection> – ПОЛУЧИ скидку 10%</PromoInfoHeader>
              <PromoText>Зарегистрируйся и получи промокод со скидкой 10% на один из следующих заказов</PromoText>
            </PromoInfo>
          </Col>
        </Row>
        <PromoMenuBlock></PromoMenuBlock>
        <div style={{paddingLeft: '40px'}}>
          {/*<OurBenefitsComponent></OurBenefitsComponent>*/}
          <HowToOrderComponent />
          
          <OurResponsibility />
        </div>
        <LoginModal
          isOpen={isLoginModalOpen}
          onRequestClose={onClickLoginBtnHandler}
        />
      </Wrapper>
  )
}

export default MainPageContent
