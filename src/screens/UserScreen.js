import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'

import {Grid, Row, Col, media} from '../components/kit/Grid'
import {theme} from '../components/kit/theme'
import HeaderComponent from '../components/common/HeaderComponent'
import FooterComponent from '../components/common/FooterComponent'
import { UserHeader } from './User/UserHeader'
import { UserMenu } from './User/UserMenu'
import { OrderItem } from './User/Orders/OrderItem'

import { getOrdersByUser } from '../AC'
import { SubHeader } from './User/styled'

class UserScreen extends Component { // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {

  }

  render() {
    return (
      <div>
        <HeaderComponent />
        <Grid style={{ fontFamily: "'Fira Sans Extra Condensed', sans-serif" }}>
          <UserHeader />
          <Row>
            <UserMenu />
            <Col size={8}>
              <div>
                <SubHeader>Текущий заказ</SubHeader>
                <h1>У Вас сейчас нет активных заказов</h1>
                {/*<SubHeader>Текущий заказ</SubHeader>
                <OrderItem />*/}
              </div>

              <div>
                <SubHeader>История заказов</SubHeader>
                <h1>Вы еще не делали заказов</h1>
                {/*<SubHeader>История заказов</SubHeader>
                <OrderItem />*/}
              </div>
            </Col>
          </Row>
        </Grid>
        <FooterComponent />
      </div>
    );
  }
}


export default connect((state) => {
  return {
    user: state.user
  }
}, { getOrdersByUser })(UserScreen);
