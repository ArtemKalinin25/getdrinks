import React, { Component } from 'react'
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom'
import styled from 'styled-components'
import {Provider} from 'react-redux'
import Modal from 'react-modal'
import CookiesModal from './components/common/Modals/CookiesModals'

import store from './store';
import { USER_ACEPT_COOKIES, REDUX_GETDRINKS } from './constans'

import MainPageScreen from './screens/MainPageScreen'
import CategoryScreen from './screens/CategoryScreen'
import SubCategoryScreen from './screens/SubCategoryScreen'
import ProductScreen from './screens/ProductScreen'
import CartScreen from './screens/CartScreen'
import SubCategoryByTypeScreen from './screens/SubCategoryByTypeScreen'
import PickUpOrderFinal from './screens/PickUpOrderFinalScreen'
import SumulateBank from './screens/SimulateBankScreen'
import SuccessPaid from './screens/SuccessPaidScreen'
import TermsScreen from './screens/TermsScreen'
import ReturnScreen from './screens/ReturnScreen'
import FinancialScreen from './screens/FinancialScreen'
import AboutUsScreen from './screens/AboutUsScreen'
import DeliveryFinalSuccess from './screens/DeliveryFinalSuccess'
import ProductsByCollection from './screens/ProductsByCollection'
import NotFoundScreen from './screens/NotFoundScreen'
import PointsComponent from './screens/PointsComponent'
import ProductsByTagName from './screens/ProductsByTagName'
import UserAgreement from './screens/UserAgreementScreen'
import UserScreen from './screens/UserScreen'

import { YMInitializer } from 'react-yandex-metrika';
import { theme } from './components/kit/theme'
import FilterProducts from "./components/common/FilterProducts/FilterProducts";

const Wrapper = styled.div`
  width: 100%;
  position: relative;
  height: 100%;

  @media (max-width: ${theme.sizes.mobileL}) {
    overflow-x: hidden;
  }
`


class App extends Component {

  state = {
    isCookiesModalOpen: true
  }

  componentDidMount() {
    this.checkUserAceptCookie()
  }

  checkUserAceptCookie = () => {
    let userAceptCookie = localStorage.getItem(USER_ACEPT_COOKIES)
    userAceptCookie = JSON.parse(userAceptCookie)
    if (userAceptCookie) {
      this.setState({isCookiesModalOpen: false})
    }
  }

  closeCookiesApproval = () => {
    this.setState({isCookiesModalOpen: false})
  }

  render() {
  return (
    <Provider store={store}>
      <Router>
        <Wrapper>
          <Switch>
            <Route exact path="/" render={(props) => <MainPageScreen {...props} />} />
            <Route exact path="/points" render={(props) => <PointsComponent {...props} />} />
            <Route exact path="/catalog/:category/byTag/:tagname" render={(props) => <ProductsByTagName {...props} />} />
            <Route exact path="/catalog/collections/:collection" render={(props) => <ProductsByCollection {...props} />} />
            <Route exact path="/catalog/:category" render={(props) => <CategoryScreen {...props} />} />
            <Route exact path="/catalog/:category/country/:country" render={(props) => <SubCategoryScreen {...props} />} />
            <Route exact path="/catalog/:category/:subcategory" render={(props) => <SubCategoryScreen {...props} />} />
            <Route exact path="/catalog/:category/:subcategory/:product" render={(props) => <ProductScreen {...props} />} />
            <Route exact path="/cart" render={(props) => <CartScreen {...props} />} />
            <Route exact path="/pickup-success" render={(props) => <PickUpOrderFinal {...props} />} />
            <Route exact path="/delivery-success" render={(props) => <DeliveryFinalSuccess {...props} />} />
            <Route exact path="/simulatebank" render={(props) => <SumulateBank {...props} />} />
            <Route exact path="/successpaid" render={(props) => <SuccessPaid {...props} />} />
            <Route exact path="/llegal" render={(props) => <TermsScreen {...props} />} />
            <Route exact path="/return" render={(props) => <ReturnScreen {...props} />} />
            <Route exact path="/about" render={(props) => <AboutUsScreen {...props} />} />
            <Route exact path="/financial" render={(props) => <FinancialScreen {...props} />} />
            <Route exact path="/useragreement" render={(props) => <UserAgreement {...props} />} />
            <Route exact path="/user" render={(props) => <UserScreen {...props} />} />
            <Route exact path="/filters" component={FilterProducts} />  {/*Временное решение*/}
            {/*<Route component={NotFoundScreen} />*/}
          </Switch>
          <YMInitializer accounts={[62412622]} options={{webvisor: true}}/>
          <CookiesModal isOpen={this.state.isCookiesModalOpen} close={this.closeCookiesApproval} />
        </Wrapper>
      </Router>
    </Provider>
  )
  }
}

export default App;
