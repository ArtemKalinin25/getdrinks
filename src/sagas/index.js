import {takeEvery, all, put, take, call} from 'redux-saga/effects'
import {GET_PRODUCTS_BY_CATEGORY__API, FETCH_CATEGORIES_LIST__API,
  SERVER_ADDR, START, SUCCESS, FAIL, CATEGORIES_API, PRODUCTS_API, GET_ONE_SCU_BY_PRODUCTS__API,
  GET_PICKPOINT, PICKPOINTS_API, SUBCATEGORY_BY_COUNTRY_API, GET_PRODUCTS_BY_SUBCATEGORY__API,
  SUBCATEGORY_BY_TYPE_API, GET_PRODUCTS_BY_COLLECTION__API, COLLECTIONS_API, GET_PRODUCTS_BY_RECOMENDATION__API,
  RECOMENDATION_API, FETCH_TAGS_LIST__API, TAGS_API, GET_PRODUCT_BY_TAG_NAME, GET_DETAIL_TAG,
  CHECK_PROMOCODE, PROMOCODES_API, REGISTER_USER, REGISTER_API, LOGIN_API,
  LOGIN_USER, GET_USER_DATA, VALIDATE_API, LLEGAL_API, GET_LLEGAL_DATA_BY_ID} from '../constans'
//import {showLoader} from '../AC'


export function* rootSaga() {
  yield all([
    getCategoriesListWorker(),
    sagaProductsWorker(),
    sagaOneProductWorker(),
    getPickpointsListWorker(),
    sagaProductsBySubCategoryWorker(),
    sagaProductsByCollectionWorker(),
    sagaProductsByRecomendationWorker(),
    sagaTagsWorker(),
    sagaProductsByTagName(),
    sagaOneTagInfoWorker(),
    sagaCheckPromoCodeWorker(),
    sagaRegisterUserWorker(),
    sagaLoginUserWorker(),
    sagaGetUserDataWorker(),
    sagaGetLlegalDataWorker()
  ])
}


export function* sagaProductsWorker() {
  yield takeEvery(GET_PRODUCTS_BY_CATEGORY__API, getProductsByCategoryWorker)
}

export function* sagaTagsWorker() {
  yield takeEvery(FETCH_TAGS_LIST__API, getTagsBuCategoryWorker)
}

export function* sagaOneTagInfoWorker() {
  yield takeEvery(GET_DETAIL_TAG, getOneTagInformationWorker)
}

export function* sagaProductsByTagName() {
  yield takeEvery(GET_PRODUCT_BY_TAG_NAME, getProductsByTagNameWorker)
}

export function* sagaOneProductWorker() {
  yield takeEvery(GET_ONE_SCU_BY_PRODUCTS__API, getOneProduct)
}

export function* sagaProductsByCollectionWorker() {
  yield takeEvery(GET_PRODUCTS_BY_COLLECTION__API, getProductsByCollectionWorker)
}

export function* sagaPickPointsWorker() {
  yield takeEvery(GET_PICKPOINT, getPickpointsListWorker)
}

function* sagaProductsBySubCategoryWorker() {
  yield takeEvery(GET_PRODUCTS_BY_SUBCATEGORY__API, getProductsBySubCategoryWorker)
}

function* sagaProductsByRecomendationWorker() {
  yield takeEvery(GET_PRODUCTS_BY_RECOMENDATION__API, getProductsByRecomengationWorker)
}

function* sagaCheckPromoCodeWorker() {
  yield takeEvery(CHECK_PROMOCODE, checkPromocode)
}

function* sagaRegisterUserWorker() {
  yield takeEvery(REGISTER_USER, registerUser)
}

function* sagaLoginUserWorker() {
  yield takeEvery(LOGIN_USER, loginUser)
}

function* sagaGetUserDataWorker() {
  yield takeEvery(GET_USER_DATA, getUserData)
}

function* sagaGetLlegalDataWorker() {
  yield takeEvery(GET_LLEGAL_DATA_BY_ID, getLlegalByKeyname)
}



function* getLlegalByKeyname({ payload }) {
  const uri = LLEGAL_API + '/' + payload
  yield abstractGetDataWorker(uri, GET_LLEGAL_DATA_BY_ID)
}

function* getUserData({ payload }) {
  console.log("TOKEN", payload)
  yield abstractPOSTDataWorker(VALIDATE_API, GET_USER_DATA, true, payload)
}

function* loginUser({ payload }) {
  yield abstractPOSTDataWorker(LOGIN_API, LOGIN_USER, true, payload)
}

function* registerUser({ payload }) {
  yield abstractPOSTDataWorker(REGISTER_API, REGISTER_USER, true, payload)
}

function* checkPromocode({ payload }) {
  console.log("checkPromocode----", payload)
  const data = { name: payload }
  yield abstractPOSTDataWorker(PROMOCODES_API, CHECK_PROMOCODE, true, data)
}


function* getOneTagInformationWorker({ payload }) {
  const uri = TAGS_API + '/taginfo/' + payload
  yield abstractGetDataWorker(uri, GET_DETAIL_TAG)
}


function* getProductsByTagNameWorker({ payload }) {
  const uri = PRODUCTS_API + '/byTag/' + payload.category + "/" + payload.tag
  console.log("PAYLOAD", payload, uri)
  yield abstractGetDataWorker(uri, GET_PRODUCT_BY_TAG_NAME)
}

function* getTagsBuCategoryWorker({ payload }) {
  const uri = TAGS_API + "/" + payload
  console.log("URI", uri)
  yield abstractGetDataWorker(uri, FETCH_TAGS_LIST__API)
}

function* getProductsByRecomengationWorker({ payload }) {
  const uri = RECOMENDATION_API + "/" + payload.category + "/" + payload.subcategory
  yield abstractGetDataWorker(uri, GET_PRODUCTS_BY_RECOMENDATION__API)
}


function* getProductsByCollectionWorker({ payload }) {
  const uri = COLLECTIONS_API + "/" + payload
  console.log("444444444", uri)
  yield abstractGetDataWorker(uri, GET_PRODUCTS_BY_COLLECTION__API)
}


function* getProductsBySubCategoryWorker({ payload }) {
  console.log(payload)
  let uri;
  switch (payload.type) {
    case "byCountry":
      uri = SUBCATEGORY_BY_COUNTRY_API + payload.category + "/" + payload.id
      break
    case "byType":
      uri = SUBCATEGORY_BY_TYPE_API + payload.id
      break
  }
  yield abstractGetDataWorker(uri, GET_PRODUCTS_BY_SUBCATEGORY__API)
}


function* getCategoriesListWorker() {
  yield abstractGetDataWorker(CATEGORIES_API, FETCH_CATEGORIES_LIST__API)
}

function* getPickpointsListWorker() {
  yield abstractGetDataWorker(PICKPOINTS_API, GET_PICKPOINT)
}

function* getProductsByCategoryWorker({ payload }) {
  console.log(payload)
  const uri = PRODUCTS_API + '/v01/' + payload
  yield abstractGetDataWorker(uri, GET_PRODUCTS_BY_CATEGORY__API)
}

export function* getOneProduct({ payload }) {
  console.log(payload)
  const uri = PRODUCTS_API + '/getProduct/' + payload
  yield abstractGetDataWorker(uri, GET_ONE_SCU_BY_PRODUCTS__API)
}

export function* abstractGetDataWorker(apiUri, ACTIONConst, isPost, data) {
  yield put({ type: ACTIONConst + START })
  try {
    let payload;
    if (isPost && data) {
        payload = yield call(postData, apiUri, data)
    } else {
        payload = yield call(getApiData, apiUri)
    }
    yield put({ type: ACTIONConst + SUCCESS, payload })
  } catch(e) {
    yield put({ type: ACTIONConst + FAIL })
  }
}


export function* abstractPOSTDataWorker(apiUri, ACTIONConst, isPost, data) {
  //console.log("???????????????", data)
  yield put({ type: ACTIONConst + START })
  let payload
  try {
    payload = yield call(postData, [apiUri, data])
    //console.log("payload", payload)
    yield put({ type: ACTIONConst + SUCCESS, payload })
    if (ACTIONConst === LOGIN_USER) {
      window.location.reload()
      //console.log(LOGIN_USER, payload)
    }

    if (ACTIONConst === REGISTER_USER) {
      window.location.reload()
      //console.log(LOGIN_USER, payload)
    }

  } catch(e) {
    //console.log("ERROR", e, e.message)
    payload = e.message
    yield put({ type: ACTIONConst + FAIL, payload })
  }
}

async function getApiData(uri) {
  const responce = await fetch(uri)
  return await responce.json()
}

async function postData(arr) {
  const [uri, data] = arr
  console.log("!!!!!!!!!!!!!!", uri, data)
  const resp = await fetch(uri, {
    method: "POST",
    headers: new Headers({
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }),
    body: JSON.stringify(data)
  })

  console.log("RESP CLEAR", resp)
  if (resp.status === 200 || resp.status === 201) {
    const respData = await resp.json()
    console.log("RESPONCE DATA", respData)
    return respData
  } else {
    const err = await resp.json()
    throw new Error(err.message)
  }

}
