import styled from 'styled-components'
import { theme } from '../theme'

/*******************/
//  Props
//  background: default: props.theme.color.green
//  height: default: 30px
//
/*******************/

export const Button = styled.div`
  background: ${props => props.background || theme.color.green};
  height:  ${props => props.height || '30px'};
  border-radius: 5px;
  border: ${props => props.border || 'none'};
  width: ${props => props.width || '80%'};
  margin-top: 15px;
  color: ${props => props.color || theme.color.white};
  text-align: center;
  font-weight: ${props => props.fontWeight || 'bold'};
  font-size: ${props => props.fontSize || '18px'};
  padding-top: ${props => props.paddingTop || '9px'};
  padding-bottom: ${props => props.paddingBottom || '9px'};
  padding-left: 5px;
  cursor: pointer;
  margin-left: ${props => props.marginLeft || '1px'};
  :hover {
    background: ${props => props.hoverColor || theme.color.darkGreen};
  }
  :before {
    content: " ";
    width: ${props => props.showIcon ? '20px' : ''};
    height: 20px;
    background: url('/img/common/basket.svg') no-repeat;
    background-size: contain;
    position: absolute;
    margin-left: -28px;
  }
`

export const Input = styled.input`
  width: 90%;
  height: 30px;
  margin-bottom: 15px;
  border-radius: 5px;
  border-width: 1px;
  border-style: solid;
  border-color: ${props => props.error ? theme.color.red : props.border || theme.color.gray };
  color: ${props => props.error ? theme.color.red : theme.color.black};
`

export const Label = styled.label`
  display: block;
  font-size: 15px;
  color: ${props => props.color || theme.color.darkGray};
  margin-bottom: 5px;
`
