import React, { Component } from 'react';
import Zoom from 'react-img-zoom'
/**
 * ZoomComp
 */
export class ZoomComp extends Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Zoom
        img={this.props.img}
        zoomScale={3}
        width={640}
        height={420}
        transitionTime={0.5}
      />
    );
  }
}

ZoomComp.propTypes = {
  //prop: PropTypes.type.isRequired
}

export default ZoomComp;
