import React from 'react'
import styled, {ThemeProvider} from 'styled-components'

import {theme} from '../../kit/theme'
import {Row, Col} from '../../kit/Grid'


const Wrapper = styled.div`
  min-height: 200px;
  margin-top: 20px;
`

const Header = styled.h2`
  text-align: center;
`

const SpeckHeader = styled.h3`
  color: ${theme.color.darkGray};
  font-weight: lighter;
  margin-top: 10px;
  margin-bottom: 5px;
  margin-left: 5px;
`

const SpeckData = styled.td`
  padding: 5px;
  padding-left: -5px;
  color: ${theme.color.black};
  border-bottom: 1px solid ${theme.color.gray};
`

//const SpeckV


const Specks = (props) => {

  return (
    <ThemeProvider theme={theme}>
      <Wrapper>
        { props.specs && props.specs.length > 0 ? (
          <h2>Характеристики</h2>
        ) : (
          <></>
        ) }

        <Row>
          <Col size={1}>
            { props.specs && props.specs.length > 0 ? (
              <SpeckHeader>Основные</SpeckHeader>
            ) : (
              <></>
            ) }

            <table>
              { props.specs  ? props.specs.map(item => {
                return(
                  <tr key={item.keyname}>
                    <SpeckData>{item.name}</SpeckData>
                    <SpeckData>{item.value}</SpeckData>
                  </tr>
                )
              }) : (<></>) }
            </table>
          </Col>
          <Col size={1}>
            { props.characteristic && props.characteristic.length > 0 ? (
              <SpeckHeader>Питательтная и энергетическая ценность</SpeckHeader>
            ) : (
              <></>
            ) }
              <table>
                { props.characteristic ? props.characteristic.map(item => {
                  return(
                    <tr key={item.keyname}>
                      <SpeckData>{item.name}</SpeckData>
                      <SpeckData>{item.value}</SpeckData>
                    </tr>
                  )
                }) : (<></>) }
              </table>
          </Col>
        </Row>
      </Wrapper>
    </ThemeProvider>
  )
}

export default Specks
