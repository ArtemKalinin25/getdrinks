import React from 'react'
import PropTypes from 'prop-types'
import styled, {ThemeProvider} from 'styled-components'

import {theme} from '../kit/theme'
import {Wrapper} from '../kit/Grid'

const BenefitsWrapper = styled.div`
  margin-top: 50px;
  margin-bottom: 80px;
  margin-left: 20px;
  height: 100%;
`

const BenefitsItem = styled.div`
  margin-right: 40px;
  position: relative;
  min-width: 200px;
  @media (max-width: ${theme.sizes.mobileL}) {
    width: 100%;
    margin-bottom: 60px;
    margin-right: 1px;
    min-width: 0;
  }
`

const Header = styled.h1`
  font-size: 30px;
  margin-bottom: 30px;
  text-align: center;
  text-transform: uppercase;
  @media (max-width: ${theme.sizes.mobileL}) {
    margin-bottom: 60px;
  }
`

const ItemHeader = styled.h3`
  margin-bottom: 10px;
  height: 45px;
  z-index: 2;
  position: relative;
`

const ItemIcon = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  margin-left: -35px;
  margin-top: -20px;
  width: 60px;
  height: 60px;
  background: url(${props => props.icon}) no-repeat;
  z-index: 1;
`

const ItemText = styled.p`
  font-size: 15px;
  font-weight: bold;
  line-height: 1.4em;
  margin-top: 30px;
`

const Row = styled.div`
  display: flex;
  @media (max-width: ${theme.sizes.mobileL}) {
    min-width: 100%;
    min-height: 290px;
    display: flex;
    overflow-x: auto;
    padding-left: 50px;
    margin-left: -30px;
    padding-top: 20px;
  }
`

const Col = styled.div`
  flex: ${(props) => props.size};
  @media (max-width: ${theme.sizes.mobileL}) {
    display: inline-block;
    margin-bottom: 30px;
    margin-right: 40px;
    min-width: 230px;
    padding-right: 20px;
  }
`;

const BenefitsCopy = (props) => {
  return (
    <ThemeProvider theme={theme}>
      <Wrapper style={{marginTop: '40px'}}>
        <Header>Почему нам стоит доверять?</Header>
        <Row inlineBlock='xs'>
          <Col size={1} inlineBlock='xs'>
            <BenefitsItem>
              <ItemIcon icon="/img/common/smile.svg"></ItemIcon>
              <ItemHeader>ЛУЧШИЕ ЦЕНЫ</ItemHeader>
              <ItemText>Минимальная цена, которую определяет производитель или поставщик, избегая наценок посредников и магазинов, предоставляется за счет продажи упаковками.</ItemText>
            </BenefitsItem>
          </Col>
          <Col size={1} inlineBlock='xs'>
            <BenefitsItem>
              <ItemIcon icon="/img/common/delivery.svg"></ItemIcon>
              <ItemHeader>НАЛИЧИЕ ПРОДУКЦИИ<br /> В ПУНКТАХ ВЫДАЧИ И ОПЛАТЫ</ItemHeader>
              <ItemText>Мы гарантируем наличие продукции через 24 часа после оформления резерва в выбранном вами пункте выдачи и оплаты в Москве и Московской области в течение 2-х дней</ItemText>
            </BenefitsItem>
          </Col>
          <Col size={1} inlineBlock='xs'>
            <BenefitsItem>
              <ItemIcon icon="/img/common/freshlike.svg"></ItemIcon>
              <ItemHeader>САМЫЙ СВЕЖИЙ ПРОДУКТ</ItemHeader>
              <ItemText>У нас представлены самые свежие напитки и пиво. Продукция поступает в пункты выдачи и оплаты непосредственно от производителя или поставщика, минуя распределительные центры и склады</ItemText>
            </BenefitsItem>
          </Col>
          <Col size={1} inlineBlock='xs'>
            <BenefitsItem>
              <ItemIcon icon="/img/common/dino.svg"></ItemIcon>
              <ItemHeader>МЫ ПРОФЕССИОНАЛЫ</ItemHeader>
              <ItemText>Мы работаем с крупнейшими поставщиками и импортерами пива и напитков в Москве. У нас широкий ассортимент продукции со всего мира, от популярных марок воды до уникальных сортов крафтового пива</ItemText>
            </BenefitsItem>
          </Col>
        </Row>
      </Wrapper>
    </ThemeProvider>
  )
}

export default BenefitsCopy
