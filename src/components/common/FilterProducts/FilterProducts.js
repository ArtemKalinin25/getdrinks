import React, { Component } from 'react';
import { connect } from "react-redux";
import FilterItem  from './FilterItem.js';
import Preloader from "./Preloader/Preloader";

//Компонент Фильтров товара
class FilterProducts extends Component {

    //Метод который приводит данные о товаре в соответсиве для построения фильтров товара
    responseDataCategoryProducts() {
        let dataFilter = [{}]; //Все необходимые данные для фильтра
        let country = []; //Страны происхождения марки товара
        let specs = []; //Данные по крепости всех товаров
        let maxValueAlc = []; //Градусы напитка
        let qunPack = []; //Количество товара в упаковке
        let typePack = [];
        let allFilters; //Итоговый готовый массив

        this.props.productsCategorySelect.products.entitys.map((item) => {
            dataFilter.push({
                brandId: item.brand_id,
                brandName: item.brand_name,
                categoryId: item.category_id,
                categoryName: item.category_name,
                categoryNameid: item.category_nameid,
                countryId: item.country_id,
                countryName: item.country_name,
                countryNameId: item.country_nameId,
                iaAlc: item.isAlc,
                packQuantity: item.pack_quantity,
                packageId: item.package_id,
                packageName: item.package_name,
                packageNameId: item.package_nameId,
                filterSelect: false,
                filterSelectCategory: false,
                specs: item.specs[0],
                subcategoryId: item.subcategory_id,
                subcategoryName: item.subcategory_name,
                idCategoryBrand: item.subcategory_name + item.brand_name
            });
        })
        dataFilter.map((item) => {
            //Формирование массива для категории страны
            country.push({
                countryId: item.countryId,
                countryName: item.countryName,
                filterSelect: item.filterSelect,
                // idFilter: item.idFilter
            })
            //Формирование массива вид упаковки
            typePack.push({
                packageName: item.packageName,
                packageNameId: item.packageNameId,
                filterSelect: item.filterSelect,
                // idFilter: item.idFilter
            })
            //Формируем массив для получения крепости товара (нужен для бегунка - фильтрация по крепости)
            specs.push(item.specs);
            //Формируем массив для получения количества товара в упаковке (нужен для бегунка - фильтрация по количество товара в упаковке)
            qunPack.push({
               packBlock: item.packQuantity
            })

        })

        //Так как приходит массив крепости вытаскиваем только крепость самого товара (есть еще сусла и так далее)
        for (let key in specs) {
            if (specs[key]) {
                maxValueAlc.push({
                    valueAlc: specs[key].value
                })
            }
        }

        //Функция фильтрует и оставляет только уникальные значения в переданном массиве
        function printUniqueResults (arrayOfObj, key) {
            return arrayOfObj.filter((item, index, array) => {
                return array.map((mapItem) => mapItem[key]).indexOf(item[key]) === index
            })
        }

        //Так как для построения фильтров нужны только уникальные значения фильтруем массивы
        country = printUniqueResults(country, 'countryId');
        maxValueAlc = printUniqueResults(maxValueAlc, 'valueAlc');
        qunPack = printUniqueResults(qunPack, "packBlock");
        typePack = printUniqueResults(typePack, "packageName")
        dataFilter = printUniqueResults(dataFilter, "subcategoryName")

        //Итоговый объект со всеми необходимымми данными для построения фильтров
        allFilters = {
            country: country.filter(country => country.countryId !== undefined),
            valueAlc: maxValueAlc,
            qunPack: qunPack.filter(qunPack => qunPack.packBlock !== undefined),
            sortArrCategory: dataFilter,
            typePack: typePack.filter(typePack => typePack.packageName !== undefined)
        }

        return allFilters
    }

    render() {
        let filtersProducts = this.responseDataCategoryProducts();
        if (filtersProducts.country.length) {
            return (
                <div className="filterProducts__wrapper">
                    <FilterItem dataFilters={this.responseDataCategoryProducts()} />
                </div>
            )
        }
        return (
            <div className="filterProducts__wrapper">
                <Preloader/>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        productsCategorySelect: state,
    }
}

export  default connect(mapStateToProps, null)(FilterProducts);