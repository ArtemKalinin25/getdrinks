import React, { Component } from 'react';
import * as _ from 'lodash';
import { connect } from "react-redux";
import {
    getFiltersProducts,
    filterCountry,
    filterCategoryProducts,
    filterBrandProducts,
    filterValueAlc,
    filterMinVolumeChange,
    filterMaxVolumeChange,
    filterSelectPack,
    filterMinPackChange,
    filterMaxPackChange,
    filterSelectTypePack,
    getSelectAllFilters
} from "../../../AC";
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';
import "./filterItem.css";

//Компонент Категории фильтра
class FilterItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            alcohol: { min: 0, max: 10 },
            pack: { min: 2, max: 24 },
            isVisibleCountry: true,
            isVisibleAllCategory: false,
            isVisibleVolumeAlc: true,
            isVisibleSelectPack: true,
            isVisiblePakType: true
        }

    }

    componentDidMount() {
        this.props.getFiltersProducts(this.props.dataFilters)
    }

    //Сортировка по категориям
    categoryRender = (categoryArr) => {
        let groupCategory = _.groupBy(categoryArr.sortArrCategory, "subcategoryName"); //Метод groupBy из библиотеки lodash
        let arrGroupCategory = [];
        for (let key in groupCategory) {
            if (key !== "undefined") {
                arrGroupCategory.push({
                    nameCategory: key,
                    brands: {
                        arrBrands: groupCategory[key],
                        idCategoryBrand: categoryArr.idCategoryBrand,
                    },
                    filterSelect: false
                })
            }
        }
        return arrGroupCategory
    }

    //Фильтрация мин\макс по крепости товара
    filterMinMaxAlc = (categoryArr) => {
        let objAlc = {
            minAlc: 0,
            maxAlc: 0
        }
        if (categoryArr && categoryArr.valueAlc.length) {
            let valAlc = categoryArr.valueAlc
            let min = _.minBy(valAlc, "valueAlc");
            let max = _.maxBy(valAlc, "valueAlc");

            let dataAlcVal = {
                minAlc: min.valueAlc.replace('%', ""),
                maxAlc: max.valueAlc.replace('%', "")
            }

            return dataAlcVal
        }

        return objAlc
    }

    //Фильтр Количества упаковок
    filterPack = (categoryArr) => {
        let objPack = {
            minPack: 0,
            maxPack: 0
        }

        //По причине, что первый элемент в любом массиве возвращает undefined
        if (categoryArr && categoryArr.qunPack.length && categoryArr.qunPack.every((val, i, erray) => val.packBlock !== undefined || erray.length !== 1 )) {
            let quantityPack = categoryArr.qunPack
            let minPack = _.minBy(quantityPack, "packBlock");
            let maxPack = _.maxBy(quantityPack, "packBlock");
            let dataPack= {
                minPack: minPack.packBlock,
                maxPack: maxPack.packBlock
            }
            return dataPack
        }
        return objPack
    }

    //Основная функция которая формирует основной фильтр
    mainFilter = (categoryArr) => {
        this.categoryRender(categoryArr);
        this.filterMinMaxAlc(categoryArr);
        this.filterPack(categoryArr);

        let {minAlc, maxAlc} = this.filterMinMaxAlc(categoryArr);
        let {minPack, maxPack} = this.filterPack(categoryArr);

        let filterDesctiption = {
            arrGroupCategory: this.categoryRender(categoryArr),
            minAlc: minAlc,
            maxAlc: maxAlc,
            minPack: minPack,
            maxPack: maxPack
        }
        return filterDesctiption
    }

    //Input Содержание алкоголя
    handlerRangeAlc = (value) => {
        this.setState({
            alcohol: value
        })
        this.props.filterValueAlc(value);
        this.props.getSelectAllFilters();
    }

    //Фильтр для ввода цифр минимального содержания алкоголя
    handlerSelectAlcMin = (event, filter) => {
        let selectAlcMin = event.target.value;
        let selectAlcMax = filter.filter.maxAlc;
        this.setState({
            alcohol: {
                min: selectAlcMin,
                max: selectAlcMax
            }
        })
        this.props.filterMinVolumeChange(selectAlcMin, selectAlcMax);
        this.props.getSelectAllFilters();
    }

    //Фильтр для ввода цифр максимального содержания алкоголя
    handlerSelectAlcMax = (event) => {
        let selectAlcMax = event.target.value;
        this.setState({
            alcohol: {
                min: this.state.alcohol.min,
                max: selectAlcMax
            }
        })
        this.props.filterMaxVolumeChange(selectAlcMax);
        this.props.getSelectAllFilters();
    }

    //Input Количество в упаковке
    handlerRangePack = (value) => {
        this.setState({
            pack: value
        })
        this.props.filterSelectPack(value);
        this.props.getSelectAllFilters();
    }

    //Фильтр для ввода цифр минимального количества товара в упаковке
    handlerSelectMinPack = (event, filter) => {
        let selectPackMin = event.target.value;
        let selectPackMax = filter.filter.maxPack;
        this.setState({
            pack: {
                min: selectPackMin,
                max: selectPackMax
            }
        })
        this.props.filterMinPackChange(selectPackMin, selectPackMax);
        this.props.getSelectAllFilters();
    }

    //Фильтр для ввода цифр максимального количества товара в упаковке
    handlerSelectMaxPack = (event) => {
        let selectPackMax = event.target.value;
        this.setState({
            pack: {
                min: this.state.pack.min,
                max: selectPackMax
            }
        })
        this.props.filterMaxPackChange(selectPackMax);
        this.props.getSelectAllFilters();
    }

    //Выбор фильтра по Странам
    handlerChangeFilterCountry = (countryName) => {
        let selectFilter = countryName.item.countryName;
        this.props.filterCountry(selectFilter);
        this.props.getSelectAllFilters();
    };

    //Выбор фильтра по Категории (название категории)
    handlerChangeCategory = (nameCategory) => {
        let selectFilter = nameCategory.item.nameCategory;
        this.props.filterCategoryProducts(selectFilter);
        this.props.getSelectAllFilters();
    }

    //Выбор фильтра по бренду
    handlerChangeFilterBrand = (brandName) => {
        console.log(brandName)
        let selectFilter = brandName.brand.brandId;
        this.props.filterBrandProducts(selectFilter);
        this.props.getSelectAllFilters();
    }

    //Выбор фильтра по Типу Упаковки
    handlerChangeFilterPackType = (idFilter) => {
        let selectFilter = idFilter;
        this.props.filterSelectTypePack(selectFilter.item.packageNameId)
        this.props.getSelectAllFilters();
    }

    //Клик по конкретной категории товара
    visibleBrands = (index) => {
        let filterItemBrand = document.getElementsByClassName('filterItem__wrapper-brand');
        filterItemBrand[index].classList.toggle("visible")
    }

    //Функция Render разметки фильтров
    renderFilter(filter) {
        return (
            <div>
                {/*Фильтр по странам*/}
                <div className="filterItem__filter-country">
                    <p className="filterItem__filter__title" onClick={() => this.setState({isVisibleCountry: !this.state.isVisibleCountry})}>Страны</p>
                    <div className={this.state.isVisibleCountry ? "filterItem__list-filter visible" : "filterItem__list-filter"}>
                        {this.props.dataFilters.country.map((item, index) => {
                            return (
                                <div className="filterItem__wrapper-item">
                                    <label>
                                        <input type="checkbox" className="filterItem__inputCheckbox" onChange={() => this.handlerChangeFilterCountry({item})}/>
                                        <span className="filterItem__nameFilter">{item.countryName}</span>
                                    </label>
                                </div>
                            )
                        })}
                    </div>
                </div>
                {/*Фильтр по категориям*/}
                <div className="filterItem__filter-category">
                    <p className="filterItem__filter__title" onClick={() => this.setState({isVisibleAllCategory: !this.state.isVisibleAllCategory})}>Категории</p>
                    <div className={this.state.isVisibleAllCategory ? "filterItem__filter-allCategory visible" : "filterItem__filter-allCategory"}>
                        {filter.arrGroupCategory.map((item, index) => {
                            return (
                                <div>
                                    <div className="filterItem__wrapper-item">
                                        <label>
                                            <input type="checkbox" className="filterItem__inputCheckbox" onChange={() => this.handlerChangeCategory({item})}/>
                                            <span
                                                className="filterItem__nameFilter filterItem__nameFilter_categoryTitle"
                                                onClick={() => this.visibleBrands(index)}
                                            >
                                                {item.nameCategory}
                                            </span>
                                        </label>
                                    </div>
                                    <div className="filterItem__wrapper-brand" >
                                        {item.brands.arrBrands.map((brand) => {
                                            return (
                                                <div className="filterItem__wrapper-item">
                                                    <label>
                                                        <input type="checkbox" className="filterItem__inputCheckbox" onChange={() => this.handlerChangeFilterBrand({brand})}/>
                                                        <span className="filterItem__nameFilter filterItem__nameFilter_brand">{brand.brandName}</span>
                                                    </label>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
                {/*Фильтр Содержание Алкоголя*/}
                <div className="filterItem__filter-category">
                    <p className="filterItem__filter__title" onClick={() => this.setState({isVisibleVolumeAlc: !this.state.isVisibleVolumeAlc})}>Содержание алкоголя</p>
                    <div className={this.state.isVisibleVolumeAlc ? "filterItem__list-filter visible" : "filterItem__list-filter"}>
                        <div className="filter">
                            <div className="filter-range-wrapper">
                                <InputRange
                                    step={0.5}
                                    maxValue={filter.maxAlc}
                                    minValue={0}
                                    value={this.state.alcohol}
                                    onChange={(value) => this.handlerRangeAlc(value)} />
                            </div>
                            <div className="filterItem__wrapper-range-input">
                                <div className="filterItem__input-wrapper-range">
                                    <label>от</label>
                                    <input type="text" onChange={(event ) => this.handlerSelectAlcMin(event, {filter})}/>
                                </div>
                                <div className="filterItem__input-wrapper-range">
                                    <label>до</label>
                                    <input type="text" onChange={(event ) => this.handlerSelectAlcMax(event)}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*Фильтр Количество в упаковке*/}
                <div className="filterItem__filter-category">
                    <p className="filterItem__filter__title" onClick={() => this.setState({isVisibleSelectPack: !this.state.isVisibleSelectPack})}>Количество в упаковке</p>
                    <div className={this.state.isVisibleSelectPack ? "filterItem__list-filter visible" : "filterItem__list-filter"}>
                        <div className="filter-range-wrapper">
                            <InputRange
                                step={1}
                                maxValue={filter.maxPack}
                                minValue={filter.minPack}
                                value={this.state.pack}
                                onChange={value => this.handlerRangePack(value)} />
                        </div>
                        <div className="filterItem__wrapper-range-input">
                            <div className="filterItem__input-wrapper-range">
                                <label>от</label>
                                <input type="text"  onChange={(event ) => this.handlerSelectMinPack(event, {filter})}/>
                            </div>
                            <div className="filterItem__input-wrapper-range">
                                <label>до</label>
                                <input type="text"  onChange={(event ) => this.handlerSelectMaxPack(event, {filter})}/>
                            </div>
                        </div>
                    </div>
                </div>
                {/*Фильтр по типу упаковки*/}
                <div className="filterItem__filter-typePack">
                    <p className="filterItem__filter__title" onClick={() => {this.setState({isVisiblePakType: !this.state.isVisiblePakType})}}>Вид упаковки</p>
                    <div className={this.state.isVisiblePakType ? "filterItem__list-filter visible" : "filterItem__list-filter"}>
                        {this.props.dataFilters.typePack.map((item, index) => {
                            return (
                                <div className="filterItem__wrapper-item">
                                    <label>
                                        <input type="checkbox" className="filterItem__inputCheckbox" onChange={() => this.handlerChangeFilterPackType({item})}/>
                                        <span className="filterItem__nameFilter">{item.packageName}</span>
                                    </label>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }

    render() {
        const filter = this.mainFilter(this.props.dataFilters);

        if (this.props.dataFilter) {
            console.log(this.props.selectAllFilters); //Здесь все выбранные фильтры
        }

        return (
            <div className="filterItem__wrapper">
                {this.renderFilter(filter)}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        dataFilter: state.products.dataFilter,
        alcVolumeProduct: state.products.alcVolumeProduct,
        selectPack: state.products.selectPack,
        selectAllFilters: state.products.selectAllFilters
    }
}

function mapDispatchToProps(dispatch) {
    return {
        filterCountry: (payload) => dispatch(filterCountry(payload)),
        getFiltersProducts: (payload) => dispatch(getFiltersProducts(payload)),
        filterCategoryProducts: (payload) => dispatch(filterCategoryProducts(payload)),
        filterBrandProducts: (payload) => dispatch(filterBrandProducts(payload)),
        filterValueAlc: (payload) => dispatch(filterValueAlc(payload)),
        filterMinVolumeChange: (minVolume, maxVolume) => dispatch(filterMinVolumeChange(minVolume, maxVolume)),
        filterMaxVolumeChange: (maxVolume) => dispatch(filterMaxVolumeChange(maxVolume)),
        filterSelectPack: (payload) => dispatch(filterSelectPack(payload)),
        filterMinPackChange: (minPack, maxPack) => dispatch(filterMinPackChange(minPack, maxPack)),
        filterMaxPackChange: (maxPack) => dispatch(filterMaxPackChange(maxPack)),
        filterSelectTypePack: (payload) => dispatch(filterSelectTypePack(payload)),
        getSelectAllFilters: () => dispatch(getSelectAllFilters())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FilterItem);
