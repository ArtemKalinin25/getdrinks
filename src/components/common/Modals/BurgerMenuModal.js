import React, { useState } from 'react'
import PropTypes from 'prop-types'
import Modal from 'react-modal';
import styled, {ThemeProvider} from 'styled-components'
import { CSSTransition } from 'react-transition-group'
import { REDUX_GETDRINKS } from '../../../constans'

import {theme} from '../../kit/theme'
import {Row, Col} from '../../kit/Grid'
import {Button} from '../../kit/styled-components/controls'
import LogoComponent from '../../common/LogoComponent'
import MenuComponent from '../../common/MenuComponent'
import LoginModal from './LoginModal'

//import './css/burger.css'



const ModalStyles = {
  overlay: {
      position: 'fixed',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      zIndex: 50
  },
  content : {
    bottom                : 'auto',
    right                 : 'auto',
    left                  :  '0',
    top                   : '0',
    width                 : '100%',
    height                : '800px',
    border                : '0',
    backgroundColor       : 'rgba(0, 0, 0, 1)',
  }
};

const CloseBtn = styled.span`
  width: 40px;
  height: 40px;
  border-radius: 40px;
  position: relative;
  z-index: 1;
  margin: 20px auto;
  cursor: pointer;
  :before {
    content: '+';
    color: ${props => props.theme.color.white};
    position: absolute;
    z-index: 2;
    transform: rotate(45deg);
    font-size: 50px;
    line-height: 1;
    top: -5px;
    left: 6px;
    transition: all 0.3s cubic-bezier(0.77, 0, 0.2, 0.85);
  }
`

const Column = styled.div`
  flex: ${(props) => props.size};
`


const LoginBlock = styled.div`
  box-sizing: border-box;
  margin-top: 10px;
  padding: 10px;
  color: ${theme.color.green};
  border: 1px solid ${theme.color.green};
  text-align: center;
  border-radius: 5px;
  cursor: pointer;
  vertical-align: middle;
  width: 60%;
  margin-left: 40px;
`

const LoginSign = styled.span`
  display: inline-block;
  margin-top: -15px;
  margin-left: 2px;
`

const UserBlock = styled.div`
  background: ${theme.color.green};
  color: ${theme.color.black};
  box-sizing: border-box;
  height: 145px;
  margin-top: -30px;
  padding-top: 50px;
  padding-left: 10px;
  padding-right: 10px;
  font-size: 14px;
`

const UserMail = styled.a`
  display: block;
  text-decoration: none;
  font-size: 18px;
  font-weight: 400;
`

const UserPic = styled.div`
  background-repeat: no-repeat;
  background-image: url(/img/users/default-avatar.svg);
  background-color: #ffffff;
  background-size: 30px 30px;
  background-position: center center;
  width: 40px;
  height: 40px;
  box-sizing: border-box;
  padding: 10px;

`

const LogoutBlock = styled.p`
  margin-top: 20px;
  font-size: 18px;
  text-decoration: underline;
  cursor: pointer;
  box-sizing: border-box;
  margin-right: 15px;
`


const Link = styled.a`
  color: ${theme.color.white};
`


const BurgerMenu = (props) => {
  const { userData, authData } = props
  const [isLoginModalOpen, toogleLoginModal] = useState(false)

  const onClickLoginBtnHandler = () => {
    toogleLoginModal(!isLoginModalOpen)
  }

  const LogoutHandler = () => {
    localStorage.removeItem(REDUX_GETDRINKS)
    window.location.reload()
  }

  return (
    <ThemeProvider theme={theme}>
      <Modal
        isOpen={props.isOpen}
        style={ModalStyles}
        onRequestClose={props.close}
        shouldCloseOnOverlayClick={false}
        closeTimeoutMS={200}
      >

          <Row>
            <Col size={1}>
              <CloseBtn onClick={props.close} />
            </Col>
            <Col size={4}>
              <LogoComponent />
            </Col>
          </Row>
          <Row>
            <MenuComponent
              isHorisontal={false}
              items={props.items}
            />
          </Row>

          <Row>
            <Column size={1} style={{ marginBottom: '10px', marginTop: '40px', marginLeft: '20px'}}>
              <p><Link href="/points">Пункты продаж в Москве <br />и Московской области</Link></p>
            </Column>
          </Row>

          <Row style={{ marginTop: '30px', marginLeft: '-20px'}}>
            <Column size={1}>
              { userData.loaded && userData.data.user ?
                (
                  <UserBlock>
                    <Row>
                        <p>{userData.data.user.name}</p>
                        <UserMail href="/user">{userData.data.user.email}</UserMail>
                    </Row>
                  </UserBlock>
                )
                : (<></>) }
                { !authData.loaded || userData.errors ? (
                  <LoginBlock onClick={onClickLoginBtnHandler} >
                    <LoginSign>Войти</LoginSign>
                  </LoginBlock>
                ) : (<></>) }
            </Column>
          </Row>

          <Row>
            <Column size={1}>
              { userData.loaded && userData.data.user ?
                (<LogoutBlock  hide="xs" onClick={LogoutHandler}>Выйти</LogoutBlock>)
                : (<></>) }
            </Column>
          </Row>
      </Modal>
      <LoginModal
        isOpen={isLoginModalOpen}
        onRequestClose={onClickLoginBtnHandler}
      />
    </ThemeProvider>
  )
}

export default BurgerMenu
