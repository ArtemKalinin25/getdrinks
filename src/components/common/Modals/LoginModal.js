import React, { Component } from 'react';
import { connect } from 'react-redux'
import Modal from 'react-modal'
import styled from 'styled-components'

import {Button, Input, Label} from '../../kit/styled-components/controls'
import {Row, Col} from '../../kit/Grid'
import { theme } from '../../kit/theme'
import { registerUser, loginUser, clearRegErrors } from '../../../AC'
import { emailValidate } from '../../../helpers/validators'

const modalStyles = {
  overlay: {
      position: 'fixed',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      zIndex: 9998
  },
  content : {
    bottom                : 'auto',
    right                 : 'auto',
    left                  :  '25%',
    top                   : '10%',
    width                 : '40%',
    height                : '300px',
    border                : '0',
    backgroundColor       : 'rgba(255, 255, 255, 1)',
    zIndex: 9999
  }
};


const CloseBnt = styled.p`
  cursor: pointer;
`

const ErrorMsg = styled.p`
  color: ${theme.color.red};
`


const passValidate = (pass) => {
  if (!pass) return false

  return pass.length > 5
}


class LoginModal extends Component { // eslint-disable-line react/prefer-stateless-function

  state = {
    email: null,
    password: null,
    formError: null,
    errors: {
      email: {
        status: false,
        message: null
      },
      password: {
        status: false,
        message: null
      }
    }
  }


  componentDidMount() {
    const { loading, loaded, errors, responce } = this.props.register
    if (errors) {
      this.setState(prevState => ({
        formError: responce
      }))
    }
  }


  setEmailError = () => {
    this.setState(prevState => ({
      errors: {
        ...prevState.errors,
        email: {
          status: true,
          message: "Неправильный формат emaila"
        }
      }
    }))
  }


  clearEmailError = () => {
    this.setState(prevState => ({
      errors: {
        ...prevState.errors,
        email: {
          status: false,
          message: null
        }
      }
    }))
  }


  setPassError = () => {
    this.setState(prevState => ({
      errors: {
        ...prevState.errors,
        password: {
          status: true,
          message: "Пароль должен быть больше 5 символов"
        }
      }
    }))
  }


  clearPassError = () => {
    this.setState(prevState => ({
      errors: {
        ...prevState.errors,
        password: {
          status: false,
          message: null
        }
      }
    }))
  }


  authHelper = (func) => {
    const { email, password } = this.state

    if (!emailValidate(email)) {
      this.setEmailError()
      return false
    }

    if (!passValidate(password)) {
      this.setPassError()
      return false
    }

    const data = { email, password }
    func(data)
    //this.close()
  }

  registerBtnHandler = () => {
    this.authHelper(this.props.registerUser)
  }

  loginBtnHandler = () => {
    this.authHelper(this.props.loginUser)
  }

  onChangeHandler = (e) => {
    const { name, value } = e.target
    this.setState({ [name] : value })
  }

  onFocusHandler = (type) => {
    switch (type) {
      case 'email':
        this.clearEmailError()
        break
      case 'password':
        this.clearPassError()
        break

      default:
        return false
    }
  }

  emailOnBlur = (e) => {
    const {value} = e.target
    if (!emailValidate(value)) {
      this.setEmailError()
    } else {
      this.clearEmailError()
    }
  }


  passOnBlur = (e) => {
    const {value} = e.target
    if (!passValidate(value)) {
      this.setPassError()
    } else {
      this.clearPassError()
    }
  }

  close = () => {
    this.props.clearRegErrors()
    this.clearEmailError()
    this.clearPassError()
    this.setState({ email: '', password: '' })
    this.props.onRequestClose()
  }

  render() {
    const { isOpen, onRequestClose, login } = this.props
    const { loading, loaded, errors, responce } = this.props.register
    const { email, password, formError } = this.state
    // console.log("errors", errors, responce)
    return (
      <Modal
        isOpen={isOpen}
        style={modalStyles}
        onRequestClose={onRequestClose}
        shouldCloseOnOverlayClick={true}
        closeTimeoutMS={200}
      >
        <div>
          <Row>
            <Col size={6}>

              { formError ? (<p>Ошибка заполнения формы</p>) : (<></>) }
              { loading ? (<p>Регистрация пользователя</p>) : (<></>) }
              { loaded ? (<p>Пользователь зарегистрирован</p>) : (<></>) }
              { errors ? (<p>{responce}</p>) : (<></>) }

              { login.errors ? (<p>{login.errorText}</p>) : (<></>) }
            </Col>
            <Col size={1}>
              <CloseBnt onClick={this.close}>
                <i class="material-icons">close</i>
              </CloseBnt>
            </Col>
          </Row>
          <Row>
            <Col size={1}>
              <Label>Email</Label>
              { this.state.errors.email.status ?
                  (<ErrorMsg>{this.state.errors.email.message}</ErrorMsg>) : (<></>)
              }
              <Input
                type="text"
                value={email}
                name="email"
                onChange={this.onChangeHandler}
                onBlur={this.emailOnBlur}
                error={this.state.errors.email.status}
                onFocus={() => this.onFocusHandler('email')}
              />

              <Label>Пароль</Label>
                { this.state.errors.password.status ?
                    (<ErrorMsg>{this.state.errors.password.message}</ErrorMsg>) : (<></>)
                }
              <Input
                type="password"
                value={password}
                name="password"
                onChange={this.onChangeHandler}
                onBlur={this.passOnBlur}
                error={this.state.errors.password.status}
                onFocus={() => this.onFocusHandler('password')}
              />
            </Col>
          </Row>
          <Row>
            <Col size={1}>
              <Button
                width="70%"
                background="#FFFFFF"
                color="#04F18C"
                border="1px solid #04F18C"
                height="25px"
                hoverColor="#FFFFFF"
                fontSize="16px"
                onClick={this.registerBtnHandler}
              >Зарегистрироваться</Button>
            </Col>
            <Col size={1}>
              <Button
                width="70%"
                height="25px"
                fontSize="16px"
                onClick={this.loginBtnHandler}
              >Войти</Button>
            </Col>
          </Row>
        </div>
      </Modal>
    );
  }
}


export default connect((state) => {
  return {
    register: state.register,
    login: state.auth
  }
}, { registerUser, loginUser, clearRegErrors })(LoginModal);
