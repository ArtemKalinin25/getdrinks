import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import styled, {ThemeProvider} from 'styled-components'
import {BASKET_LOCAL_STORAGE_NAME} from '../../../constans'

import {getCartFromLocalStorage, checkPromoCode} from '../../../AC'
import { Label, Button, Input } from './styled-components'

import CardProductItem from './CardProductItemComponent'

class CartProductsList extends Component {

  state = {
    basket: [],
    promocode: ""
  }

  componentDidMount() {
    this.getDataFromBasket()
    this.props.getCartFromLocalStorage()

  }

  getDataFromBasket = () => {
    let storageData = localStorage.getItem(BASKET_LOCAL_STORAGE_NAME)
    storageData = JSON.parse(storageData)
    this.setState({basket: storageData})
  }

  increaseQuantity = (e, id) => {
    //console.log(id);
    const {basket} = this.state
    let result = basket.map(item => {
      if (id === item[0].scu_id) {
        //console.log(item[0])
        //console.log("!!!!!!!!!!", item[0].stock, (Number(item[0].quantity) + 1), (Number(item[0].quantity) + 1) > item[0].stock)

        if ( ((item[0].quantity * item[0].pack_quantity) + item[0].pack_quantity) < item[0].stock ) {
          item[0].quantity = Number(item[0].quantity) + 1
          item[0].total = (Number(item[0].total) + Number(item[0].price)).toFixed(2)
          return item
        } else {
          alert("Большего количества сейчас нет на складе")
          return item
        }
      } else {
        return item
      }
    })
    localStorage.setItem(BASKET_LOCAL_STORAGE_NAME, JSON.stringify(result))
    this.props.getTotalSumm()
    this.props.getSumProducts()
    this.setState({basket: result})
  }

  decreaseQuantity = (e, id) => {
    const {basket} = this.state
    let result = basket.map(item => {
      if (id === item[0].scu_id && item[0].quantity > 1) {
        //console.log(item[0])
        item[0].quantity--
        item[0].total -= item[0].price
        return item
      } else {
        return item
      }
    })
    localStorage.setItem(BASKET_LOCAL_STORAGE_NAME, JSON.stringify(result))
    this.props.getTotalSumm()
    this.props.getSumProducts()
    this.setState({basket: result})
  }

  changeQuantity = (e, id) => {
    const {basket} = this.state
    let result = basket.map(item => {
      if (id === item[0].scu_id && item[0].quantity > 1) {
        //console.log(item[0])
        item[0].quantity = e.target.value
        item[0].total = item[0].price * item[0].quantity
        return item
      } else {
        return item
      }
    })
    localStorage.setItem(BASKET_LOCAL_STORAGE_NAME, JSON.stringify(result))
    this.props.getTotalSumm()
    this.props.getSumProducts()
    this.setState({basket: result})
  }

  deleteItem = (id) => {
    //alert("Delete");
    const {basket} = this.state;
    let result = basket.filter(item => {
      if (item[0].scu_id !== id) {
        return item;
      }
    });
    localStorage.setItem(BASKET_LOCAL_STORAGE_NAME, JSON.stringify(result))
    this.props.getTotalSumm()
    this.props.getSumProducts()
    this.setState({basket: result})
    //window.location.reload()
  }

  generateItems = () => {
    const basket = this.state.basket
    console.log("basket", basket)
    if (Array.isArray(basket)) {
    const result = basket.map(item => {
      let packshoot = item[0].packshoot.url ? item[0].packshoot[0] : '/img/noimage.jpg'
      const price = (item[0].price * 1).toFixed(2)
      return(
        <CardProductItem
          scuName={item[0].scu_name}
          scuId={item[0].scu_id}
          packshoot={packshoot.url}
          oldTotal={item[0].isPromo ? item[0].oldTotal : false}
          price={price}
          pricePerUnit={item[0].price_by_one}
          sale={item[0].sale}
          discountPrice={item[0].discount_price}
          stock={item[0].stock}
          packName={item[0].package_name}
          mdId={item[0].md_id}
          quantity={item[0].quantity}
          increase={this.increaseQuantity}
          decrease={this.decreaseQuantity}
          changeQuantity={this.changeQuantity}
          deleteItem={this.deleteItem}
          total={item[0].total}
          frozenQuantity={item[0].frozenQuantity || false}
        />
      )
    })
    return result;
  } else {
    const result = <h1>В резерве нет продуктов</h1>
    return result;
  }

  }


  checkPromoCodeBtnHandler = () => {
    const { promocode } = this.state
    console.log("promocode", this.state.promocode)
    this.props.checkPromoCode(this.state.promocode)

  }

  inputPromoChangeHandler = (e) => {
    //console.log("____-----")
    this.setState({ promocode: e.target.value })
  }

  applyDiscount = (discount) => {
    const {basket} = this.state
    const { loadedPromocode } = this.props
    let regPromo = false
    if (loadedPromocode.loaded && loadedPromocode.entitys && loadedPromocode.entitys.regpromo) {
      //alert("REG PROMO")
      regPromo = true
    }

    let result = basket.map(item => {
      if (!item[0].isPromo) {
        item[0].oldPrice_by_one = (Number(item[0].price_by_one)).toFixed(2)
        item[0].price_by_one = (Number(item[0].price_by_one) - (Number(item[0].price_by_one) / 100 * discount)).toFixed(2)
        item[0].oldPrice = (Number(item[0].price)).toFixed(2)
        item[0].oldTotal = (Number(item[0].total)).toFixed(2)
        item[0].price = (Number(item[0].price) - (Number(item[0].price) / 100 * discount)).toFixed(2)
        item[0].total = (Number(item[0].price) * Number(item[0].quantity)).toFixed(2)
        item[0].isPromo = true
        item[0].promoName = this.state.promocode
        item[0].regpromo = regPromo
      } else {
        //alert("Промокод уже применен")
      }
      return item
    })
    localStorage.setItem(BASKET_LOCAL_STORAGE_NAME, JSON.stringify(result))
    this.props.getTotalSumm()
    this.props.getSumProducts()
    //this.setState({basket: result})
    //window.location.reload()
  }

  render() {
    console.log(this.props)
    const { basket, promocode } = this.state
    const { loadedPromocode } = this.props
    console.log("basket999", basket)

    if (this.props.loadedPromocode.loaded && this.props.loadedPromocode.entitys) {
      this.applyDiscount(+this.props.loadedPromocode.entitys.discount)
    }

    return (
      <>
        {this.generateItems()}
        <div>
          { basket && basket.length > 0 ?
            (<div>
              <Label>Ввести промокод</Label>
                { loadedPromocode.errors ? (<p>Ошибка в промокоде</p>) : (<></>) }
                { loadedPromocode.entitys === false ? (<p>Ошибка в промокоде</p>) : (<></>) }
                { loadedPromocode.loaded && loadedPromocode.entitys ?
                  (<p>Промокод применен, скидка {loadedPromocode.entitys.discount} %</p>)
                  : (<></>) }
              <Input
                type="text"
                value={this.state.promocode}
                name="promocode"
                onChange={this.inputPromoChangeHandler}
              />
              <Button
                onClick={this.checkPromoCodeBtnHandler}
              >
                Применить
              </Button>
            </div>)
            : (<></>)
          }
        </div>
      </>
    )
  }
}



export default connect((state) => {
  return {
    cart: state.cart.entitys,
    loadedPromocode: state.promocodes
  }
}, {getCartFromLocalStorage, checkPromoCode})(CartProductsList)
