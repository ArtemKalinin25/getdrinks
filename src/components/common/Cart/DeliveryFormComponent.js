import React from 'react'
import PropTypes from 'prop-types'
import styled, {ThemeProvider} from 'styled-components'
import { ReactDadata } from 'react-dadata';

import {theme} from '../../kit/theme'
import {Row, Col} from '../../kit/Grid'

import {Wrapper, Input, Selection, Label, TextArea} from './styled-components'
import PayTypeChoose from './PayTypeChooseComponent'
//import {generateDeliveryDate} from '../../../helpers/timers'

const Delivery = styled.h4`
  margin-bottom: 20px;
  margin-top: 10px;
  font-size: 18px;
  font-weight: normal;
`

const BeforeFreeDelivery = styled.p`
  font-size: 14px;
  margin-top: 5px;
`

const NameBlock = styled.div`
  margin-top: 20px;
`

const Select = styled.select`
  width: 90%;
  height: 30px;
  margin-bottom: 15px;
  border-radius: 5px;
  border: 1px solid ${props => props.theme.color.darkGray};
`


const DeliveryForm = (props) => {

  //console.log("!!!!!!!!!!!!!", props);

  const checkCostDelivery = () => {
    if (props.totalSum > 1500) {
      return <Delivery>Этот заказ мы <Selection>доставим Вам бесплатно</Selection></Delivery>
    } else {
      return (
        <Delivery>
          Стоимость доставки - 350 ₽.
          <BeforeFreeDelivery>Для бесплатной доставки закажите еще товаров на {1500 - props.totalSum} ₽.</BeforeFreeDelivery>
        </Delivery>
      )
    }
  }

  const generateDeliveryDate = () => {
    let mounths = 'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,девакбря'.split(',')
    let today = new Date();
    let tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
    let monthTomorrow = tomorrow.getMonth() + 1;
    let result = []
    let count = [1, 3, 5, 7, 9, 11];
    for (let i = 1; i < 6; i++) {
        let lastDay = count.indexOf(monthTomorrow*1) != 1 ? 31 : 30;
        let date = new Date(today.getTime() + ((24 * 60 * 60 * 1000))*i).getDate();
        if (date == lastDay && monthTomorrow < 12) {
            monthTomorrow++;
        }
        let showDate = date + ' ' + mounths[monthTomorrow - 1]
        if (date < 10) {
          date = '0' + date
        }
        let value = date + "/" + "0"+monthTomorrow + "/" + tomorrow.getFullYear()
        result.push({value: value, date: showDate})
    }
    return result;
  }
  let dates = generateDeliveryDate()
  //console.log("333333333", dates)


  return (
    <ThemeProvider theme={theme}>
      <Wrapper>
        {checkCostDelivery()}
        <div>
          <Label>Адрес доставки</Label>
          <ReactDadata
              token="136b1ccb95958feafedb85b85b6a79fe35d3dc33"
              placeholder="Начните вводить ваш адрес"
              onChange={(e) => {
                  props.addressChangeHandler(e)
              }}
          />
          <Row>
            <Col size={1}>
              <Label>Код домофона</Label>
              <Input type="text" onChange={props.userInputHandler} value={props.domofoneValue} name="domofone" />
            </Col>
            <Col size={1}>
              <Label>Этаж</Label>
              <Input type="text" onChange={props.userInputHandler} value={props.levelValue} name="level" />
            </Col>
          </Row>
          <Row>
            <Col size={1}>
              <Label>Ближайшая дата доставки</Label>
              <Select onChange={props.dateDeliveryChangeHandler} value={props.dateDelivery}>
                  /* написано в 5 утра, 2fz ночь без сна заебался  */
                  <option value="08/05/2020">8 мая</option>
              </Select>
            </Col>
            <Col size={1}>
              <Label>Время доставки</Label>
                <Select onChange={props.timeDeliveryChangeHandler} value={props.timeDelivery}>
                  <option value="firstPart">C 8:00 до 16:00</option>
                  <option value="secondPart">C 16:00 до 22:00</option>
                </Select>
            </Col>
          </Row>
          <NameBlock>
            <Label>Ваше имя</Label>
            <Input type="text" value={props.nameValue} onChange={props.userInputHandler} name="name" />
          </NameBlock>
          <Row>
            <Col size={1}>
              <Label>Телефон</Label>
              <Input type="text" onChange={props.userInputHandler} value={props.phoneValue} name="phone" />
            </Col>
            <Col size={1}>
              <Label>E-mail</Label>
              <Input type="text" onChange={props.userInputHandler} value={props.emailValue} name="email" />
            </Col>
          </Row>
          <Row>
            <Col size={1}>
              <Label>Комментарии</Label>
              <TextArea onChange={props.userInputHandler} value={props.commentsValue} name="comments"></TextArea>
            </Col>
          </Row>
          <PayTypeChoose methodPay={props.methodPay} changeMethodPay={props.changeMethodPay} />
        </div>
      </Wrapper>
    </ThemeProvider>
  )
}

export default DeliveryForm
