import React, { Component } from 'react'
//import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled, {ThemeProvider} from 'styled-components'
import { getAllPickPoints } from '../../../AC'
import { getDeliveryDate, getTodayDate } from '../../../helpers/timers'

import {Wrapper, Label, NameBlock, TextArea} from './styled-components'
import { Input } from '../../kit/styled-components/controls'

import {theme} from '../../kit/theme'
import {Row, Col} from '../../kit/Grid'
import PayTypeChoose from './PayTypeChooseComponent'
import { points } from '../../../fakeData/points'
import { emailValidate } from '../../../helpers/validators'

const Header = styled.h2`
  margin-bottom: 20px;
`

const PickUpChoise = styled.div`
  margin: 10px;
  margin-left: 0px;
  background: ${props => props.theme.color.gray};
  padding: 10px;
`

const PickUpChoiseHeader = styled.p`
  font-weight: bold;
  margin-bottom: 5px;
`

const PickUpPointsList = styled.div`
  margin-right: 10px;
  overflow-x: auto;
  height: 280px;
`

const PickUpListItem = styled.div`
  padding: 5px;
  padding-top: 9px;
  padding-bottom: 9px;
  border-top: 1px solid ${props => props.theme.color.gray};
  border-bottom: 1px solid ${props => props.theme.color.gray};
  cursor: pointer;
  :hover {
    background: ${props => props.theme.color.gray};
  }
`

const ErrorMsg = styled.p`
  color: ${theme.color.red};
`

const coords = points
/**
 * PickUpForm
 */
class PickUpForm extends Component { // eslint-disable-line react/prefer-stateless-function


  pickUpItem = React.createRef()

  componentDidMount() {
    this.props.getAllPickPoints()
    window.addEventListener('load', this.handleLoad);
  }

  onHandlerClickList = (e) => {
    //const ref = e.currentTarget
    const pointId = e.currentTarget.dataset.id
    const address = e.currentTarget.childNodes[2].innerText
    const name = e.currentTarget.childNodes[0].innerText
    //console.log(pointId, address, name)
    this.props.pickPointHandler(pointId, address, name)
    //console.log("123", e.currentTarget.childNodes[2])
  }

  handleLoad = () => {
    window.ymaps.ready(() => {
      this.localMap = new window.ymaps.Map('map', {center: [55.753661, 37.619774], zoom: 10}, {
        searchControlProvider: 'yandex#search'});

        for (let i = 0; i < coords.length; i++) {
          var placemark = new window.ymaps.Placemark(coords[i].coords, {
              iconContent: coords[i].point_id,
              hintContent : coords[i].net_name + " " + points[i].address
            }, {
              //preset: "islands#circleDotIcon",
              //iconColor: '#00cc00',
              iconLayout: 'default#image',
              iconImageHref: 'img/common/mapicon.gif',
              iconImageSize: [30, 30],
              iconImageOffset: [-5, -28]
          });
          placemark.name = coords[i].address;
          placemark.events.add(['click'],  (e) => {
            const address = e.originalEvent.target.name
            const name = e.originalEvent.target.properties._data.hintContent
            const id = e.originalEvent.target.properties._data.iconContent
            //this.setState({ pickupPointId: id, pickupPointAddr: address, pickupPointName: name })
            //this.setState({ pickupPoint: { id, address, name } })
            console.log("ADRESS", e.originalEvent.target)
            this.props.pickPointHandler(id, address, name)
          })
          this.localMap.geoObjects.add(placemark);
        }
    });
  }

  emailOnBlur = (e) => {
    const {value} = e.target
    if (!emailValidate(value)) {
      this.props.setEmailError()
    } else {
      this.props.clearEmailError()
    }
  }

  onFocusHandler = (type) => {
    switch (type) {
      case 'email':
        this.props.clearEmailError()
        break

      default:
        return false
    }
  }

  render() {
    const {pickupPoint} = this.props
    //console.log("@!@!@!@!@!@!@!@!@!@!", this.props.points)
    return (
      <ThemeProvider theme={theme}>
        <Wrapper>
          <Header>Пункты продаж</Header>
          <Row>
            <Col size={1} >
              <div>
                <h3>Откуда забрать резерв?</h3>
                <p>Выберите пункт продажи в списке ниже или на карте</p>
                <p>Зарезервированная продукция будет готова к выдаче через 24 часа
                  при оформления резерва до 17-00. Продукция будет в наличии в пункте
                  продажи в течение 2-х рабочих дней с {getDeliveryDate()} при заказе до 17.00 {getTodayDate()}</p>
              </div>
              <PickUpChoise>
                { pickupPoint.id && pickupPoint.address && pickupPoint.name ? (
                  <div>
                    <PickUpChoiseHeader>Вы выбрали</PickUpChoiseHeader>
                    <p>
                      <span>{pickupPoint.name} ,</span>
                      <span>{pickupPoint.address} ,</span>
                    </p>
                  </div>
                ) : ( <p>Пункт продажи не выбран</p> ) }
              </PickUpChoise>
              <div>
                <p>Пункты продажи</p>
                <PickUpPointsList>
                  {
                    coords.map(item => {
                        return(
                          <PickUpListItem
                            ref={this.pickUpItem}
                            onClick={(e) => this.onHandlerClickList(e)}
                            data-id={item.point_id}
                          >

                            <Label>{item.point_type} {item.net_name}</Label>
                            <Label>Время работы {item.work_time}</Label>
                            <p>{item.address}</p>
                          </PickUpListItem>
                        )
                    })
                  }
                </PickUpPointsList>
              </div>
            </Col>
            <Col size={2}>
              <Label>Выберите пункт продажна карте</Label>
              <div id='map' style={{width: '100%', height: '400px'}}></div>
            </Col>
          </Row>


          <Row>
            <Col size={1}>
              <NameBlock>
                <Label>Ваше имя</Label>
                <Input type="text" onChange={this.props.userInputHandler} value={this.props.nameValue} name="name" />
              </NameBlock>
            </Col>
          </Row>
          <Row>
            <Col size={1}>
              <Label>Телефон</Label>
              <Input type="text" onChange={this.props.userInputHandler} value={this.props.phoneValue} name="phone" />
            </Col>
            <Col size={1}>
              <Label>E-mail</Label>
                { this.props.formErrors.email.status ?
                    (<ErrorMsg>{this.props.formErrors.email.message}</ErrorMsg>) : (<></>)
                }
              <Input
                type="text"
                onChange={this.props.userInputHandler}
                value={this.props.emailValue}
                name="email"
                onBlur={this.emailOnBlur}
                error={this.props.formErrors.email.status}
                onFocus={() => this.onFocusHandler('email')}
              />
            </Col>
          </Row>
          <Row>
            <Col size={1}>
              <Label>Комментарии</Label>
              <TextArea onChange={this.props.userInputHandler} value={this.props.commentsValue} name="comments"></TextArea>
            </Col>
          </Row>
          <PayTypeChoose methodPay={this.props.methodPay} changeMethodPay={this.props.changeMethodPay} />
        </Wrapper>
      </ThemeProvider>
    )
  }
}

PickUpForm.propTypes = {
  //prop: PropTypes.type.isRequired
}

export default connect((state) => {
  return {
    points: state.pickpoints
  }
}, {getAllPickPoints})(PickUpForm)
