import React from 'react'
import PropTypes from 'prop-types'
import styled, {ThemeProvider} from 'styled-components'

const Wrapper = styled.div`
  height: 200px;
  margin-top: 20px;
`

const BestSellers = (props) => {
  return (
    <Wrapper>
      <h2>Лучшие предложения</h2>
    </Wrapper>
  )
}

export default BestSellers
