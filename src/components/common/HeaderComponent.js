import React, {useState, useEffect} from 'react'
import PropTypes from 'prop-types'
import { useSelector, shallowEqual, useDispatch } from 'react-redux'
import styled, {ThemeProvider} from 'styled-components'
import { REDUX_GETDRINKS, GET_USER_DATA } from '../../constans'
import { getUserData } from '../../AC'

import {theme} from '../kit/theme'
import {size} from '../settings/devices'
import {Grid, Row, Col} from '../kit/Grid'

import LogoComponent from './LogoComponent'
import MenuComponent from './MenuComponent'
import BasketComponent from './Header/BasketComponent'
import BurgerMenu from './Modals/BurgerMenuModal'
import LoginModal from './Modals/LoginModal'

const media = {
  xs: (styles) => `
    @media only screen and (max-width: ${theme.sizes.mobileL}) {
      ${styles}
    }
  `,
  lg: (styles) => `
    @media only screen and (min-width: ${theme.sizes.tablet}) {
      ${styles}
    }
  `,
}

const Header = styled.div`
  height: ${props => props.height || '165px'};
  width: 100%;
  background: ${props => props.theme.color.black};
  color: ${props => props.theme.color.white};
  position: relative;
  @media (max-width: ${theme.sizes.mobileL}) {
    width: 100%;
    height: 165px;
  }
`

const Column = styled.div`
  flex: ${(props) => props.size};

  ${(props) => props.hide && media[props.hide](`
    display: none;
  `)};
`

const Wrapper = styled.div`
  padding-left: 60px;
  padding-top: ${props => props.menuPaddingTop};

  @media (max-width: ${theme.sizes.mobileL}) {
    padding-left: 20px;
    padding-top: 20px;
  }
`;

const DeliveryInfo = styled.p`
  margin-left: 20%;
  color: ${props => props.theme.color.darkGray};

  @media (max-width: ${theme.sizes.mobileL}) {
    margin-left: 0;
    font-size: 14px;
  }
`;

const BurgerIcon = styled.span`
  position: relative;
  display: inline-block;
  width: 50%;
  height: 1.2em;
  margin-right: 0.6em;
  border-top: 0.2em solid #fff;
  border-bottom: 0.2em solid #fff;
  :before {
    content: "";
    position: absolute;
    top: 0.5em;
    left: 0px;
    width: 100%;
    border-top: 0.2em solid #fff;
  }
`

const LoginBlock = styled.div`
  box-sizing: border-box;
  margin-top: 10px;
  padding: 10px;
  color: ${theme.color.green};
  border: 1px solid ${theme.color.green};
  text-align: center;
  border-radius: 5px;
  cursor: pointer;
  vertical-align: middle;
  width: 60%;
  margin-left: 40px;
`

const LoginSign = styled.span`
  display: inline-block;
  margin-top: -15px;
  margin-left: 2px;
`

const UserBlock = styled.div`
  background: ${theme.color.green};
  color: ${theme.color.black};
  box-sizing: border-box;
  height: ${props => props.height || '145px'};
  margin-top: ${props => props.marginTop};
  padding-top: ${props => props.paddingTop};
  padding-left: 10px;
  padding-right: 10px;
  font-size: 14px;
`

const UserMail = styled.a`
  display: block;
  text-decoration: none;
  font-size: 18px;
  font-weight: 400;
`

const UserPic = styled.div`
  background-repeat: no-repeat;
  background-image: url(/img/users/default-avatar.svg);
  background-color: #ffffff;
  background-size: 30px 30px;
  background-position: center center;
  width: 40px;
  height: 40px;
  box-sizing: border-box;
  padding: 10px;

`

const LogoutBlock = styled.p`
  margin-top: 20px;
  font-size: 18px;
  text-decoration: underline;
  cursor: pointer;
  box-sizing: border-box;
  margin-right: 15px;
`

const Link = styled.a`
  color: ${theme.color.white};
`

const HeaderComponent = (props) => {

  const [isMenuOpen, toogleMenu] = useState(false)
  const [isLoginModalOpen, toogleLoginModal] = useState(false)
  const categories = useSelector(state => state.categories, shallowEqual)
  const userData = useSelector(state => state.user, shallowEqual)
  const authData = useSelector(state => state.auth, shallowEqual)
  //console.log("HEADER USER", userData)
  const dispatch = useDispatch()

  useEffect(() => {
    checkUserTokent()
  }, [])

  const onClickBurgerIconHandler = () => {
    toogleMenu(!isMenuOpen)
  }

  const onClickLoginBtnHandler = () => {
    toogleLoginModal(!isLoginModalOpen)
  }

  const checkUserTokent = () => {
    let tokenArr = JSON.parse(localStorage.getItem(REDUX_GETDRINKS))
    if (tokenArr && tokenArr.loaded && tokenArr.entitys.token) {
      const data = {
        token: tokenArr.entitys.token,
        id: tokenArr.entitys.userId
      }
      dispatch({ type: GET_USER_DATA, payload: data })
    }
  }

  const LogoutHandler = () => {
    localStorage.removeItem(REDUX_GETDRINKS)
    window.location.reload()
  }

  return (
    <ThemeProvider theme={theme}>
      <Header height={props.height || '165px'}>
        <Wrapper menuPaddingTop={props.menuPaddingTop || '30px'}>
            <Row>
              <Column size={1} hide="lg">
                <BurgerIcon onClick={onClickBurgerIconHandler} />
              </Column>
              <Column size={1}>
                <LogoComponent />
              </Column>
              <Column size={7} hide="xs">
                { categories.loaded ? (
                  <MenuComponent items={categories.entitys} />
                ) : (
                  <p>Загрузка</p>
                ) }
              </Column>

              <Column size={3} hide="xs">
                { userData.loaded && userData.data.user ?
                  (
                    <UserBlock
                      height={props.topMenu ? '70px' : '145px'}
                      marginTop={props.topMenu ? '-6px' : '-30px'}
                      paddingTop={props.topMenu ? '25px' : '50px'}
                    >
                      <Row>
                          <p>{userData.data.user.name}</p>
                          <UserMail href="/user">{userData.data.user.email}</UserMail>
                      </Row>
                    </UserBlock>
                  )
                  : (<></>) }
                  { !authData.loaded || userData.errors ? (
                    <LoginBlock onClick={onClickLoginBtnHandler}>
                      <LoginSign>Войти</LoginSign>
                    </LoginBlock>
                  ) : (<></>) }
              </Column>



              <Column size={1}>
                <BasketComponent />
              </Column>

              <Column size={1} hide="xs">
                { userData.loaded && userData.data.user ?
                  (<LogoutBlock  hide="xs" onClick={LogoutHandler}>Выйти</LogoutBlock>)
                  : (<></>) }
              </Column>
            </Row>

            { !props.hidePoints ? (
              <Row>
                <Column size={3} hide="xs"></Column>
                <Column size={7} hide="xs" style={{ marginBottom: '10px', marginTop: '-1px'}}>
                  <p><Link href="/points">Пункты продаж в Москве и Московской области</Link></p>
                </Column>
              </Row>
            ) : (<></>)
            }

        </Wrapper>
        { !props.hideAlert ? (
          <div
            style={{
              textAlign: 'center',
              marginTop: '10px',
              background: '#EEEEEE',
              color: 'black',
              position: 'absolute',
              bottom: '0',
              left: '0',
              right: '0',
              fontWeight: 'bold',
              padding: '5px',
              zIndex: '20',
            }}
          >
            <p>Мы открылись в тестовом режиме! Резервируйте любимые напитки и пиво! Если что-то пошло не так, пишите <a href="mailto:hello@getdrinks.ru">hello@getdrinks.ru</a></p>
          </div>
          ) : (<></>)
        }
      </Header>
      { categories.loaded ? (
        <BurgerMenu
          isOpen={isMenuOpen}
          close={onClickBurgerIconHandler}
          items={categories.entitys}
          userData={userData}
          authData={authData}
        />
      ) : (<></>) }
      <LoginModal
        isOpen={isLoginModalOpen}
        onRequestClose={onClickLoginBtnHandler}
      />
    </ThemeProvider>
  )
}

HeaderComponent.propsTypes = {
  isMenuShow: PropTypes.boolean,
  isBasketShow: PropTypes.boolean
}

HeaderComponent.defaultProps = {
  isMenuShow: true,
  isBasketShow: true
}

export default HeaderComponent
